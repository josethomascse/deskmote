//
//  AddToDoViewController.h
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AddToDoViewControllerDelegate <NSObject>
- (void)receiveMessage:(NSString *)message1 and:(NSDate *)message2 ;
@end
@interface AddToDoViewController : UIViewController<AddToDoViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *save;
@property (weak, nonatomic) IBOutlet UITextField *itemText;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *reminderdate;
@property NSDate *reminderdate_Date;
@property NSString *reminderdate_String;
@end
