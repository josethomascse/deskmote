//
//  DatePickerViewController.m
//  deskmote
//
//  Created by user on 7/7/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "DatePickerViewController.h"
#import "SettingsControl.h"

@interface DatePickerViewController ()<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,UITextFieldDelegate>
@property (nonatomic, retain) NSString *currenttimestring;
@property NSDate *currenttime;
@end

@implementation DatePickerViewController
CGRect screenRect1;
CGFloat screenWidth1;
CGFloat screenHeight1;

UIDatePicker *datepicker1;
UITextField *textField;
- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationPopover;
        self.popoverPresentationController.delegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    screenRect1 = [[UIScreen mainScreen] bounds];
    screenWidth1 = screenRect1.size.width;
    screenHeight1 = screenRect1.size.height;


    UIView *space1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth1, 70)];
    space1.backgroundColor=[UIColor whiteColor];

    UILabel *prefixLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    [prefixLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [prefixLabel sizeToFit];
    
    textField = [[UITextField  alloc] initWithFrame:CGRectMake(25, 30, screenWidth1-70, 30)];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.contentVerticalAlignment =
    UIControlContentVerticalAlignmentCenter;
    [textField setFont:[UIFont boldSystemFontOfSize:12]];
    textField.placeholder = @"Remind Me About";
    textField.leftView = prefixLabel;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.delegate = self;
    
    datepicker1 = [[UIDatePicker alloc]initWithFrame:CGRectMake(50, 60,screenWidth1, 50)];
    datepicker1.datePickerMode = UIDatePickerModeDateAndTime;
    [datepicker1 setCenter:CGPointMake((screenWidth1/2), 90)];
    
    if(_flag==0)
    {
        datepicker1.date=_defaultdate;
    }
    [self.view addSubview: datepicker1];
        [self.view addSubview:space1];
    if(_flag==0)
    {
        [textField setText:_data];
    }
    [self.view addSubview:textField];

    UIButton *save = [[UIButton alloc]initWithFrame:CGRectMake((screenWidth1/2)-70,110 , 100, 100)];
    save.backgroundColor=[UIColor colorWithRed:248.0 green:248.0 blue:248.0 alpha:1.0];
    [save setTitle:@"SAVE"  forState:UIControlStateNormal];
    [save.titleLabel setTextAlignment:NSTextAlignmentLeft];

    [save setTitleColor:[UIColor darkGrayColor]  forState:UIControlStateNormal];
    save.tintColor=[UIColor darkGrayColor];
    [save addTarget:self action:@selector(closePopover) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *space2 = [[UIView alloc]initWithFrame:CGRectMake(0, 110, screenWidth1, 100)];
    space2.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:space2];
    [self.view addSubview:save];
    textField.layer.borderColor=[[UIColor darkGrayColor]CGColor];
    textField.layer.borderWidth=1.0;
    textField.layer.cornerRadius=6;
    
    CGRect screenRect;
    CGFloat screenCenterX,screenCenterY;
    screenRect = [[UIScreen mainScreen] bounds];
    screenCenterX = screenRect.size.width/2;
    screenCenterY = screenRect.size.height/2;
    
    // generating toast to handle with constrains

     UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenCenterX-105,datepicker1.center.y+11.5, 210, 25)];
    [self setToastLabel:tempLabel];
    [self.ToastLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:13]];
    [self.ToastLabel  setTextAlignment:NSTextAlignmentCenter];
    [self.ToastLabel  setTextColor:[UIColor whiteColor]];
    self.ToastLabel.backgroundColor =[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
    self.ToastLabel.layer.cornerRadius = 10;
    
    self.ToastLabel.layer.masksToBounds = YES;
    [_ToastLabel setHidden:YES];
    [[self view] addSubview:_ToastLabel];
    
    
    //adding listener to date picker
    [datepicker1 addTarget:self
               action:@selector(datePickerValueChanged:)
     forControlEvents:UIControlEventValueChanged];
 
    //adding listener to textfield
    [textField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];

}


//This function will be called when user change the date
- (void)datePickerValueChanged:(id)sender
{
    [_ToastLabel setText:@"Please select a Future date!"];
    UIDatePicker *picker = (UIDatePicker *)sender;
    if ([[NSDate date] compare:picker.date ]== NSOrderedAscending)
    {
        
            [_ToastLabel setHidden:YES];
    }
    else
    {
            [_ToastLabel setHidden:NO];
    }
 
    
}
-(void)textFieldDidChange:(id)sender
{
    textField.layer.borderColor=[[UIColor darkGrayColor]CGColor];
    textField.layer.borderWidth=1.0;
    textField.layer.cornerRadius=6;

}
-(NSDate *)removeSeconds:(NSDate *)date{
    NSTimeInterval time = floor([date timeIntervalSinceReferenceDate] / 60.0) * 60.0;
    return  [NSDate dateWithTimeIntervalSinceReferenceDate:time];
}

- (void)closePopover
{
            textField.layer.borderColor=[[UIColor darkGrayColor]CGColor];
    NSString *text= textField.text;
    if ([text isEqualToString:@""]) {

        textField.layer.borderColor=[[UIColor redColor]CGColor];
 
        
        [self shaketextfield];
        return;
  
    }

    if ([[NSDate date] compare:datepicker1.date ]== NSOrderedAscending)
    {
        
        if (_notify!=nil)
        {
            [[UIApplication sharedApplication] cancelLocalNotification:_notify];
        }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM,YYYY hh:mm a"];
    datepicker1.date=[self removeSeconds:datepicker1.date];
    _currenttime = datepicker1.date;
    _currenttimestring = [dateFormatter stringFromDate:datepicker1.date];
    
       
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = _currenttime;
    localNotification.alertBody = textField.text;
    localNotification.alertAction = @"Show me the item";
    
    NSDictionary *info=[NSDictionary dictionaryWithObject:textField.text forKey:@"reminder"];
    localNotification.userInfo=info;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.soundName=UILocalNotificationDefaultSoundName;
    //  localNotification.applicationIconBadgeNumber = 1;//[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
   
       
        SettingsControl *file=[[SettingsControl alloc]init];
        file=[file initialize];
        if (![file retrieveRemindersStatus]) {
            [file setreminderSwitch:TRUE];
            [file saveToFile:file];
        }
    //[self setBadgenumber];
    //[self datafromcontroller:localNotification];
        
    // Dismiss the view controller
    [self.navigationController popViewControllerAnimated:YES];
   
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
        return;
        
    }
    else
    {
        [_ToastLabel setText:@"Please select a Future date!"];
        [_ToastLabel setHidden:NO];
    }
    
}
-(void)shaketextfield
{
    

    CGPoint textorigin=CGPointMake(textField.frame.origin.x,textField.frame.origin.y);
    
    [UIView animateWithDuration:.06
                          delay:0
                        options: UIViewAnimationOptionAutoreverse
     
                     animations:^ {
                         [textField setFrame:CGRectMake(textorigin.x+6, textorigin.y, textField.frame.size.width, textField.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                         
                     }];
    [UIView animateWithDuration:.06
                          delay:.06
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                         [textField setFrame:CGRectMake(textorigin.x,textorigin.y , textField.frame.size.width, textField.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");

                     }];
    [UIView animateWithDuration:.06
                          delay:.12
                        options: UIViewAnimationOptionAutoreverse
     
                     animations:^ {
                         [textField setFrame:CGRectMake(textorigin.x+6, textorigin.y, textField.frame.size.width, textField.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                         
                     }];
    [UIView animateWithDuration:.06
                          delay:.18
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                         [textField setFrame:CGRectMake(textorigin.x,textorigin.y , textField.frame.size.width, textField.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                         textField.placeholder = @"";
                         [textField becomeFirstResponder];
                     }];



}
/*
-(void)setBadgenumber
{
    int badgeNumber=1,flag;
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSInteger count=[localNotifications count];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
    
    
    for (int i=0; i<count; i++) {
        flag=1;
        localNotification = [localNotifications objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        if (1==flag) {
            localNotification.applicationIconBadgeNumber=badgeNumber;
            badgeNumber=badgeNumber+1;
        }
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Popover Presentation Controller Delegate

- (void)prepareForPopoverPresentation:(UIPopoverPresentationController *)popoverPresentationController {
    
    if(_flag==1)
    {
    self.sourceRect = CGRectMake((screenWidth1/2), (screenHeight1/2)-12, 0, 25);
    }
    else{
      self.sourceRect = CGRectMake((screenWidth1/2)-50, (screenHeight1/2)-12, 0, 25);
    }
    self.popoverPresentationController.sourceView = self.sourceView ? self.sourceView : self.view;
    self.popoverPresentationController.sourceRect = self.sourceRect;
    
    self.preferredContentSize = CGSizeMake(400, 200);
    popoverPresentationController.permittedArrowDirections = self.arrowDirection ? self.arrowDirection : 0;
    popoverPresentationController.passthroughViews = self.passthroughViews;
    popoverPresentationController.backgroundColor = self.backgroundColor;
    popoverPresentationController.popoverLayoutMargins = self.popoverLayoutMargins;
}

#pragma mark - Adaptive Presentation Controller Delegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    
    return UIModalPresentationNone;
}


@end
