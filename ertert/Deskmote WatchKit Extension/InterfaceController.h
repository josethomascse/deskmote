//
//  InterfaceController.h
//  Deskmote WatchKit Extension
//
//  Created by user on 7/28/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *label;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *image;
@property (strong, nonatomic) NSTimer *timer;
@end
