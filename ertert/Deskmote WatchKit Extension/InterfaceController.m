//
//  InterfaceController.m
//  Deskmote WatchKit Extension
//
//  Created by user on 7/28/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController()

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
     [super awakeWithContext:context];

}


- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    
    [super willActivate];
    
    [InterfaceController openParentApplication:@{ @"reply": @"reply1" }
                                         reply:^(NSDictionary *replyInfo, NSError *error) {
                                             // NSString *a = replyInfo[@"reply"];
                                             
                                             [self.image setImageNamed:replyInfo[@"reply"]];
                                             
                                         }];
    [InterfaceController openParentApplication:@{ @"reply": @"reply2" }
                                         reply:^(NSDictionary *replyInfo, NSError *error) {
                                             NSString *a = replyInfo[@"reply"];
                                             [self.label setText:a];
                                             // Configure interface objects here.
                                         }];

 
    
        self.timer = [NSTimer scheduledTimerWithTimeInterval:5.00
                                                          target:self
                                                        selector:@selector(poolTimer)
                                                        userInfo:nil
                                                         repeats:YES];
    
    
 


}
- (void)poolTimer
{
    [InterfaceController openParentApplication:@{ @"reply": @"reply1" }
                                         reply:^(NSDictionary *replyInfo, NSError *error) {
                                            // NSString *a = replyInfo[@"reply"];
                                           
                                             [self.image setImageNamed:replyInfo[@"reply"]];
                                             
                                         }];
    [InterfaceController openParentApplication:@{ @"reply": @"reply2" }
                                         reply:^(NSDictionary *replyInfo, NSError *error) {
                                             NSString *a = replyInfo[@"reply"];
                                             [self.label setText:a];
                                                                                        // Configure interface objects here.
                                         }];
    //self.session.progressTime;
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
    [self.timer invalidate];
}

@end



