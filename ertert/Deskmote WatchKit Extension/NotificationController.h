//
//  NotificationController.h
//  Deskmote WatchKit Extension
//
//  Created by user on 7/28/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
