//
//  NotificationManager.h
//  deskmote
//
//  Created by user on 7/21/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NotificationManager : NSObject
-(void)officestartalert:(NSDate *)firedate;
-(void)officestartalert_silent:(NSDate *)firedate;
-(void)officestopalert:(NSDate *)firedate;
-(void)officestopalert_silent:(NSDate *)firedate;
-(void)lunchstartalert:(NSDate *)firedate;
-(void)lunchstartalert_silent:(NSDate *)firedate;
-(void)lunchstopalert:(NSDate *)firedate;
-(void)lunchstopalertsilent:(NSDate *)firedate;
-(void)cancel_allNotifications:(NSString *)identifier;
//-(void)setBadgenumber;
@end
