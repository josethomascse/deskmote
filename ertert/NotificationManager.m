//
//  NotificationManager.m
//  deskmote
//
//  Created by user on 7/21/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "NotificationManager.h"
#import <UIKit/UIKit.h>

@implementation NotificationManager
-(void)officestartalert:(NSDate *)firedate
{
    [self cancel_allNotifications:@"officeTimings1"];
    UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    officeTimings.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    officeTimings.fireDate=firedate;
    officeTimings.repeatInterval=NSCalendarUnitDay;
    officeTimings.alertBody = @"Office Time!";
    officeTimings.alertAction = @"Show me the item";
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"office start alert" forKey:@"officeTimings1"];
    officeTimings.userInfo=info;
    officeTimings.timeZone = [NSTimeZone defaultTimeZone];
    officeTimings.repeatInterval = NSCalendarUnitDay;
    officeTimings.soundName=UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
//    [self setBadgenumber];

}

    
-(void)officestartalert_silent:(NSDate *)firedate
{
    // For App Start
    [self cancel_allNotifications:@"startapp"];
    UILocalNotification *startapp= [[UILocalNotification alloc] init];
    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    startapp.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    startapp.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Start tha app" forKey:@"startapp"];
    startapp.userInfo=info;
    startapp.repeatInterval=NSCalendarUnitDay;
    startapp.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:startapp];
    
}


-(void)officestopalert:(NSDate *)firedate
{

    [ self cancel_allNotifications:@"officeTimings2"];
    UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
        officeTimings.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
        officeTimings.fireDate=firedate;
    officeTimings.repeatInterval=NSCalendarUnitDay;
    officeTimings.alertBody = @"Go Home!";
    officeTimings.alertAction = @"Show me the item";
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"office start alert" forKey:@"officeTimings2"];
    officeTimings.userInfo=info;
    officeTimings.timeZone = [NSTimeZone defaultTimeZone];

    officeTimings.repeatInterval = NSCalendarUnitDay;
    officeTimings.soundName=UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
//    [ self setBadgenumber];
    
    
}
-(void)officestopalert_silent:(NSDate *)firedate
{

    //For App stop
    [self cancel_allNotifications:@"stopapp"];
    UILocalNotification *stopapp= [[UILocalNotification alloc] init];
    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    stopapp.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    stopapp.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Stop the app" forKey:@"stopapp"];
    stopapp.userInfo=info;
    stopapp.repeatInterval=NSCalendarUnitDay;
    stopapp.timeZone = [NSTimeZone defaultTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:stopapp];
    
}
-(void)lunchstartalert:(NSDate *)firedate
{

    [self cancel_allNotifications:@"breakalert1"];
    UILocalNotification *breakalerts1 = [[UILocalNotification alloc] init];
     if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    breakalerts1.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    breakalerts1.fireDate=firedate;
    breakalerts1.alertBody = @"Lunch Time";
    breakalerts1.alertAction = @"Show me the item";

    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert1"];
    breakalerts1.userInfo=info;
    breakalerts1.timeZone = [NSTimeZone defaultTimeZone];
    breakalerts1.repeatInterval = NSCalendarUnitDay;
    breakalerts1.soundName=UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts1];
//    [self setBadgenumber];
}
-(void)lunchstartalert_silent:(NSDate *)firedate
{
    //SILENT NOTIFICATION FOR LUNCH START
    [self cancel_allNotifications:@"lunchstartsilent"];
    UILocalNotification *lunchstart= [[UILocalNotification alloc] init];
    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    lunchstart.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    lunchstart.fireDate=firedate;        NSDictionary *info=[NSDictionary dictionaryWithObject:@"not nil" forKey:@"lunchstartsilent"];
    lunchstart.userInfo=info;
    lunchstart.timeZone = [NSTimeZone defaultTimeZone];
    lunchstart.repeatInterval=NSCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:lunchstart];
    // Schedule the notification
    
}

-(void)lunchstopalert:(NSDate *)firedate
{
    [self cancel_allNotifications:@"breakalert2"];
    UILocalNotification *breakalerts2 = [[UILocalNotification alloc] init];

    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    breakalerts2.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    breakalerts2.fireDate=firedate;

    breakalerts2.alertBody = @"Lunch Time Over";
    breakalerts2.alertAction = @"Show me the item";

    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert2"];
    breakalerts2.userInfo=info;
    breakalerts2.timeZone = [NSTimeZone defaultTimeZone];
    breakalerts2.repeatInterval = NSCalendarUnitDay;
    breakalerts2.soundName=UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts2];
//    [self setBadgenumber];
}
-(void)lunchstopalertsilent:(NSDate *)firedate
{
    //SILENT NOTIFICATION FOR LUNCH START
    [self cancel_allNotifications:@"lunchstopsilent"];
    UILocalNotification *lunchstop= [[UILocalNotification alloc] init];

    if ([[NSDate date] compare:firedate]==NSOrderedDescending)
    lunchstop.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
    else
    lunchstop.fireDate=firedate;

    NSDictionary *info=[NSDictionary dictionaryWithObject:@"not nil" forKey:@"lunchstopsilent"];
    lunchstop.userInfo=info;
    lunchstop.timeZone = [NSTimeZone defaultTimeZone];
    lunchstop.repeatInterval=NSCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:lunchstop];

}


-(void)cancel_allNotifications:(NSString *)identifier
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification *localNotification = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:identifier]];
        if(![uid isEqualToString:@"(null)"])
        {
            
            [app cancelLocalNotification:eventArray[i]];
        }
    }
//    [self setBadgenumber];
}
/*
-(void)setBadgenumber
{
    int badgeNumber=1,flag;
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSInteger count=[localNotifications count];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
    
    
    for (int i=0; i<count; i++) {
        flag=1;
        localNotification = [localNotifications objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        if (1==flag) {
            localNotification.applicationIconBadgeNumber=badgeNumber;
            badgeNumber=badgeNumber+1;
        }
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}
*/
@end
