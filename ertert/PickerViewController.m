//
//  PickerViewController.m
//  deskmote
//
//  Created by user on 7/7/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "PickerViewController.h"
#import "SettingsControl.h"

@interface PickerViewController ()<UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate>
@property (nonatomic, retain) NSDate *pickedtime;

@end

@implementation PickerViewController
UIDatePicker *datepicker;
@synthesize sendertag;
- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationPopover;
        self.popoverPresentationController.delegate = self;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    datepicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 195, 25)];
    datepicker.datePickerMode = UIDatePickerModeTime;
    [datepicker setCenter:CGPointMake(100, 20)];
    
    UIButton *save = [[UIButton alloc]initWithFrame:CGRectMake(0,40 , 195, 80)];
    save.backgroundColor=[UIColor colorWithRed:248.0 green:248.0 blue:248.0 alpha:1.0];
    [save setTitle:@"OK"  forState:UIControlStateNormal];
    [save setTitleColor:[UIColor blackColor]  forState:UIControlStateNormal];
    
    [save addTarget:self action:@selector(closePopover) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: datepicker];
    
    [self.view addSubview:save];
}

-(NSDate *)removeSeconds:(NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd MMM yyyy HH:mm a" ];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];

    NSString *datestring=[dateFormat stringFromDate:date];
    

    
    return [dateFormat dateFromString:datestring];

}

- (void)closePopover
{

    _pickedtime=datepicker.date;
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd MMM yyyy hh:mm a" ];
    NSString *datestring=[dateFormat stringFromDate:_pickedtime];
    _pickedtime=[dateFormat dateFromString:datestring];

    SettingsControl *cat1 = [[SettingsControl alloc]init];
    cat1=[cat1 initialize];
    if(sendertag==0)
    {

       
        [cat1 setOfficeStart: _pickedtime];
        if ([cat1 retrieveOfficeTimingsStatus]==true)
        {
            [self cancel_allNotifications:@"officeTimings1"];
            UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
            if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
                officeTimings.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
            else
                officeTimings.fireDate=_pickedtime;
            officeTimings.repeatInterval=NSCalendarUnitDay;
            officeTimings.alertBody = @"Office Time!";
            officeTimings.alertAction = @"Show me the item";
            NSDictionary *info=[NSDictionary dictionaryWithObject:@"office start alert" forKey:@"officeTimings1"];
            officeTimings.userInfo=info;
            officeTimings.timeZone = [NSTimeZone defaultTimeZone];
            officeTimings.repeatInterval = NSCalendarUnitDay;
            officeTimings.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
//            [self setBadgenumber];
            
        }
        
        // For App Start
        [self cancel_allNotifications:@"startapp"];
        UILocalNotification *startapp= [[UILocalNotification alloc] init];
        if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
            startapp.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
        else
            startapp.fireDate=_pickedtime;
        NSDictionary *info=[NSDictionary dictionaryWithObject:@"Start tha app" forKey:@"startapp"];
        startapp.userInfo=info;
        startapp.repeatInterval=NSCalendarUnitDay;
        startapp.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:startapp];
 
        
    }
    else if (sendertag==1)
    {
       
        [cat1 setOfficeEnd: _pickedtime];
        if ([cat1 retrieveOfficeTimingsStatus]==true)
        {
            [self cancel_allNotifications:@"officeTimings2"];
            UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
            if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
                officeTimings.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
            else
                officeTimings.fireDate=_pickedtime;
            officeTimings.repeatInterval=NSCalendarUnitDay;
            officeTimings.alertBody = @"Go Home!";
            officeTimings.alertAction = @"Show me the item";
            NSDictionary *info=[NSDictionary dictionaryWithObject:@"office start alert" forKey:@"officeTimings2"];
            officeTimings.userInfo=info;
            officeTimings.timeZone = [NSTimeZone defaultTimeZone];
     
            officeTimings.repeatInterval = NSCalendarUnitDay;
            officeTimings.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
//                  [self setBadgenumber];
        }
    
        //For App stop
        [self cancel_allNotifications:@"stopapp"];
        UILocalNotification *stopapp= [[UILocalNotification alloc] init];
        if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
            stopapp.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
        else
            stopapp.fireDate=_pickedtime;
        NSDictionary *info=[NSDictionary dictionaryWithObject:@"Stop the app" forKey:@"stopapp"];
        stopapp.userInfo=info;
        stopapp.repeatInterval=NSCalendarUnitDay;
        stopapp.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:stopapp];
        
    }
    else if (sendertag==2){
        [cat1 setLunchStart: _pickedtime];
        if ([cat1 retrieveBreakAlertsStatus]==true)
        {
 
            [self cancel_allNotifications:@"breakalert1"];
            UILocalNotification *breakalerts1 = [[UILocalNotification alloc] init];
            NSDate *firedate=_pickedtime;
            if ([[NSDate date] compare:firedate]==NSOrderedDescending)
                breakalerts1.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
            else
                breakalerts1.fireDate=firedate;
            breakalerts1.alertBody = @"Lunch Time";
            breakalerts1.alertAction = @"Show me the item";
            
            NSDictionary *info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert1"];
            breakalerts1.userInfo=info;
            breakalerts1.timeZone = [NSTimeZone defaultTimeZone];
             breakalerts1.repeatInterval = NSCalendarUnitDay;
            breakalerts1.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts1];
//              [self setBadgenumber];
            
        }
        
        //SILENT NOTIFICATION FOR LUNCH START
        [self cancel_allNotifications:@"lunchstartsilent"];
        UILocalNotification *lunchstart= [[UILocalNotification alloc] init];
        if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
            lunchstart.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
        else
            lunchstart.fireDate=_pickedtime;        NSDictionary *info=[NSDictionary dictionaryWithObject:@"lunchstartsilent" forKey:@"lunchstartsilent"];
        lunchstart.userInfo=info;
        lunchstart.timeZone = [NSTimeZone defaultTimeZone];
        lunchstart.repeatInterval=NSCalendarUnitDay;
        [[UIApplication sharedApplication] scheduleLocalNotification:lunchstart];
    }
    else if (sendertag==3)
    {
        [cat1 setLunchEnd: _pickedtime];
        if ([cat1 retrieveBreakAlertsStatus]==true)
        {
            // Schedule the notification
            [self cancel_allNotifications:@"breakalert2"];
            UILocalNotification *breakalerts2 = [[UILocalNotification alloc] init];

            if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
                breakalerts2.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
            else
                breakalerts2.fireDate=_pickedtime;
            
            breakalerts2.alertBody = @"Lunch Time Over";
            breakalerts2.alertAction = @"Show me the item";
            
            NSDictionary *info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert2"];
            breakalerts2.userInfo=info;
            breakalerts2.timeZone = [NSTimeZone defaultTimeZone];
            breakalerts2.repeatInterval = NSCalendarUnitDay;
            breakalerts2.soundName=UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts2];
//            [self setBadgenumber];

        }
        
        //SILENT NOTIFICATION FOR LUNCH STOP
        [self cancel_allNotifications:@"lunchstopsilent"];
        UILocalNotification *lunchstop= [[UILocalNotification alloc] init];
        
        if ([[NSDate date] compare:_pickedtime]==NSOrderedDescending)
            lunchstop.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:_pickedtime];
        else
            lunchstop.fireDate=_pickedtime;
        
        NSDictionary *info=[NSDictionary dictionaryWithObject:@"not nil" forKey:@"lunchstopsilent"];
        lunchstop.userInfo=info;
        lunchstop.timeZone = [NSTimeZone defaultTimeZone];
        lunchstop.repeatInterval=NSCalendarUnitDay;
        [[UIApplication sharedApplication] scheduleLocalNotification:lunchstop];
    }
    [cat1 saveToFile:cat1];
    //UILocalNotification *lnfcn=[[UILocalNotification alloc]init];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadtime" object:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)cancel_allNotifications:(NSString *)identifier
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification *localNotification = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:identifier]];
        if(![uid isEqualToString:@"(null)"])
        {
            
            [app cancelLocalNotification:eventArray[i]];
        }
    }
//        [self setBadgenumber];
}

//-(void)setBadgenumber
//{
//    int badgeNumber=1,flag;
//    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
//    NSInteger count=[localNotifications count];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
//    
//    
//    for (int i=0; i<count; i++) {
//        flag=1;
//        localNotification = [localNotifications objectAtIndex:i];
//        NSDictionary *userInfoCurrent = localNotification.userInfo;
//        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        if (1==flag) {
//            localNotification.applicationIconBadgeNumber=badgeNumber;
//            badgeNumber=badgeNumber+1;
//        }
//        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    }
//    
//}


#pragma mark - Popover Presentation Controller Delegate

- (void)prepareForPopoverPresentation:(UIPopoverPresentationController *)popoverPresentationController {
    
    self.sourceRect = CGRectMake(0, 0, 0, 25);
    
    
    
    self.popoverPresentationController.sourceView = self.sourceView ? self.sourceView : self.view;
    self.popoverPresentationController.sourceRect = self.sourceRect;
    
    self.preferredContentSize = CGSizeMake(195, 100);
    popoverPresentationController.permittedArrowDirections = self.arrowDirection ? self.arrowDirection : UIPopoverArrowDirectionUp;
    
    popoverPresentationController.backgroundColor = self.backgroundColor;
    popoverPresentationController.popoverLayoutMargins = self.popoverLayoutMargins;
}

#pragma mark - Adaptive Presentation Controller Delegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    
    return UIModalPresentationNone;
}

@end
