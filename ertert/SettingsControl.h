//
//  SettingsControl.h
//  deskmote
//
//  Created by user on 7/2/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface SettingsControl : NSObject
@property (retain,nonatomic) AppDelegate *appDelegate;

    -(BOOL)retrieveNotificationStatus;
    -(BOOL)retrieveRemindersStatus;
    -(BOOL)retrieveBreakAlertsStatus;
    -(BOOL)retrieveHealthWarningsStatus;
    -(BOOL)retrieveOfficeTimingsStatus;
    -(BOOL) installed;
-(NSDate *)retrieveOfficeStart;
-(NSDate *)retrieveOfficeEnd;
-(NSDate *)retrieveLunchStart;
-(NSDate *)retrieveLunchEnd;
-(NSString *)retrieveusername;
-(NSString *)retrievecompanyname;
-(NSString *)retrieveProfilePic;
    -(void)saveToFile:(SettingsControl *)object;
    -(void) setnotificationswitch:(BOOL)status;
    -(void) setreminderSwitch:(BOOL)status;
    -(void) setbreakAlertsSwitch:(BOOL)status;
    -(void) sethealthWarningsSwitch:(BOOL)status;
    -(void) setofficeTimingsSwitch:(BOOL)status;
    -(void) setinstalled:(BOOL)status;
    -(BOOL)setOfficeStart:(NSDate *)time;
    -(BOOL)setOfficeEnd:(NSDate *)time;
    -(BOOL)setLunchStart:(NSDate *)time;
    -(BOOL)setLunchEnd:(NSDate *)time;
    -(void)setCurrentStatus;
    -(void)setusername:(NSString *)name;
    -(void)setcompanyname:(NSString *)name;
    -(void)setProfilePic:(NSString *)image;
-(SettingsControl *)initialize;
-(long int)getTime:(NSDate *)date;
-(BOOL)validConfiguration:(BOOL)change;
-(long int)getWorkTime;
-(long int)getWorkIntervalTime:(NSDate *)date interval:(long int)seconds;
-(BOOL)retrieveDbStatus;
@end
