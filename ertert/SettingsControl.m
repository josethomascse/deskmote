//
//  SettingsControl.m
//  deskmote
//
//  Created by user on 7/2/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "SettingsControl.h"
#import "Status.h"
#import "NotificationManager.h"
#import <UIKit/UIKit.h>


@implementation SettingsControl{
    BOOL notificationswitch;
    BOOL reminderSwitch;
    BOOL breakAlertsSwitch;
    BOOL healthWarningsSwitch;
    BOOL officeTimingsSwitch;
    BOOL install;
    NSDate *officestart,*officeend,*lunchstart,*lunchend;
    NSString *cmpnyname,*myname,*imgpath;
}
-(BOOL)installed
{
    return install;
}
-(BOOL)retrieveNotificationStatus{
    return notificationswitch;
}
-(BOOL)retrieveRemindersStatus{
    return reminderSwitch;
}
-(BOOL)retrieveBreakAlertsStatus{
    return breakAlertsSwitch;
}
-(BOOL)retrieveHealthWarningsStatus{
    return healthWarningsSwitch;
}
-(BOOL)retrieveOfficeTimingsStatus{
    return officeTimingsSwitch;
}
-(NSDate *)retrieveOfficeStart{
    return officestart;
}
-(NSDate *)retrieveOfficeEnd{
    return officeend;
}
-(NSDate *)retrieveLunchStart{
    return lunchstart;
}
-(NSDate *)retrieveLunchEnd{
    return lunchend;
}
-(NSString *)retrieveusername{
    return myname;
}
-(NSString *)retrievecompanyname{
    return cmpnyname;
}
-(NSString *)retrieveProfilePic{
    return imgpath;
}
    -(void)setusername:(NSString *)name{
        myname=name;
    }
    -(void)setcompanyname:(NSString *)name{
        cmpnyname=name;
    }
    -(void)setProfilePic:(NSString *)path{
        imgpath=path;
    }

-(void)setCurrentStatus{
    if(notificationswitch==true)
    {
        
    }
    else{
        
    }
    if(reminderSwitch==true)
    {
        
    }
    else{
        
    }
    if(breakAlertsSwitch==true)
    {
        
    }
    else{
        
    }
    if(healthWarningsSwitch==true)
    {
        
    }
    else{
        
    }
    if(officeTimingsSwitch==true)
    {
        
    }
    else{
        
    }
    
}
-(void)setinstalled:(BOOL)status
{
    install=status;
    
}

-(void)setbreakAlertsSwitch:(BOOL)status
{
    breakAlertsSwitch=status;
    
}

-(void)setofficeTimingsSwitch:(BOOL)status
{
    officeTimingsSwitch=status;
    
}
-(void)setreminderSwitch:(BOOL)status
{
    reminderSwitch=status;
}
-(void)setnotificationswitch:(BOOL)status
{
    notificationswitch=status;

}
-(void)sethealthWarningsSwitch:(BOOL)status
{
    healthWarningsSwitch=status;

}

-(long int)getTime:(NSDate *)date{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MM,yyyy"];
    NSString *datestring=[dateFormatter stringFromDate:date];
    NSDate* myDate = [dateFormatter dateFromString:datestring];
    NSTimeInterval seconds=[date timeIntervalSinceDate:myDate];
    return seconds;
}

-(BOOL)validConfiguration:(BOOL)change{
    long int start,end,lstart,lend;
    start=[self getTime:officestart];
    end=[self getTime:officeend];
    lstart=[self getTime:lunchstart];
    lend=[self getTime:lunchend];
    if(lstart<start)
        lstart+=86400;
    if(lend<start)
        lend+=86400;
    if(end<start)
        end+=86400;
    if(lstart>start && lend<end && lstart<lend)
        return 1;
    if(change){
        NotificationManager *nm=[[NotificationManager alloc]init];
        lunchstart=[officestart dateByAddingTimeInterval:60];
        if (breakAlertsSwitch)
        {
            [nm lunchstartalert:lunchstart];
        }
        [nm lunchstartalert_silent:lunchstart];
        
        lunchend=[officestart dateByAddingTimeInterval:120];
        if (breakAlertsSwitch)
        {
            [nm lunchstopalert:lunchend];
        }
        [nm lunchstopalertsilent:lunchend];
        
        if((start+180)>end)
        {
            officeend=[officestart dateByAddingTimeInterval:180];
            if (officeTimingsSwitch)
            {
                [nm officestopalert:officeend];
            }
            [nm officestopalert_silent:officeend];
        }
    }
    return 0;
}

-(BOOL)retrieveDbStatus{
    long int start,end,lstart,lend,now;
    start=[self getTime:officestart];
    end=[self getTime:officeend];
    lstart=[self getTime:lunchstart];
    lend=[self getTime:lunchend];
    now=[self getTime:[NSDate date]];
    if(lstart<start)
        lstart+=86400;
    if(lend<start)
        lend+=86400;
    if(end<start)
        end+=86400;
    if(now<start)
        now+=86400;
   
    if(now>=end || (now>=lstart && now<lend))
       return 1;
    return 0;
}


-(long int)getWorkTime{
    long int start,end,lstart,lend;
    start=[self getTime:officestart];
    end=[self getTime:officeend];
    lstart=[self getTime:lunchstart];
    lend=[self getTime:lunchend];
    if(lstart<start)
        lstart+=86400;
    if(lend<start)
        lend+=86400;
    if(end<start)
        end+=86400;
    return ((end-start)-(lend-lstart));
}

-(long int)getWorkIntervalTime:(NSDate *)date interval:(long int)seconds{
    long int A[6],refSeconds=0,interval=0;
	int add=0,index=0,canAdd[4],refIndex=0;
	A[0]=[self getTime:officestart];
	canAdd[0]=0;
	A[1]=[self getTime:lunchstart];
	canAdd[1]=1;
	A[2]=[self getTime:lunchend];
	canAdd[2]=0;
	A[3]=[self getTime:officeend];
	canAdd[3]=1;
	A[4]=[self getTime:date];
	A[5]=A[4]+seconds;
    while(1){
        index=[self findSmallestIndex:A];
		if(index == 4)
   			add=1;
     	else if(index == 5){
			long int temp=A[5];
			A[5]=99999;
			index=[self findSmallestIndex:A];
            if(A[index]!=99999){
                if(canAdd[index])
                    interval+=(temp-refSeconds);
            }
            else{
                if(!canAdd[refIndex])
                    interval+=(temp-refSeconds);
            }
			break;
		}
		else{
			if(add)
				if(canAdd[index])
					interval+=(A[index]-refSeconds);
            refIndex=index;
		}
		refSeconds=A[index];
		A[index]=99999;
	}
	return interval;
}

-(int)findSmallestIndex:(long int *)A{
	long int small=99999;
 	int i,index=0;
 	for(i=0;i<6;i++){
		if(A[i]<small){
			index=i;
			small=A[i];
			}
	}	
	return index;
}

-(BOOL)setOfficeStart:(NSDate *)time{
    
    Status *st=[[Status alloc]init];
    st=[st initialize];
    [st setCurrentStatus:[st retrieveCurrentStatus]];
    [st saveToFile:st];
    officestart=time;
    self.appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    if([self retrieveDbStatus])
        self.appDelegate.navBar.backgroundColor=[UIColor whiteColor];
    else
        self.appDelegate.navBar.backgroundColor=[st retrieve_tintcolor];
    [self validConfiguration:1];
    return 1;
    
 }


-(BOOL)setOfficeEnd:(NSDate *)time{
    
    Status *st=[[Status alloc]init];
    st=[st initialize];
    [st setCurrentStatus:[st retrieveCurrentStatus]];
    [st saveToFile:st];
    officeend=time;
    self.appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    if([self retrieveDbStatus])
        self.appDelegate.navBar.backgroundColor=[UIColor whiteColor];
    else
        self.appDelegate.navBar.backgroundColor=[st retrieve_tintcolor];
    [self validConfiguration:1];
    return 1;
}

-(BOOL)setLunchStart:(NSDate *)time{
    NSDate *temp=lunchstart;
    Status *st=[[Status alloc]init];
    st=[st initialize];
    [st setCurrentStatus:[st retrieveCurrentStatus]];
    [st saveToFile:st];
    lunchstart=time;
    if([self validConfiguration:0]){
        self.appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
        if([self retrieveDbStatus])
            self.appDelegate.navBar.backgroundColor=[UIColor whiteColor];
        else
            self.appDelegate.navBar.backgroundColor=[st retrieve_tintcolor];
        return 1;
    }
    else{
        lunchstart=temp;
        return 0;
    }

    
}
-(BOOL)setLunchEnd:(NSDate *)time{
    
     NSDate *temp=lunchend;
    lunchend=time;
    Status *st=[[Status alloc]init];
    st=[st initialize];
    [st setCurrentStatus:[st retrieveCurrentStatus]];
    [st saveToFile:st];
    if([self validConfiguration:0]){
        self.appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
        if([self retrieveDbStatus])
            self.appDelegate.navBar.backgroundColor=[UIColor whiteColor];
        else
            self.appDelegate.navBar.backgroundColor=[st retrieve_tintcolor];
        return 1;
    }
    else{
        lunchend=temp;
        return 0;
    }

}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self->notificationswitch  =[decoder decodeIntForKey:@"13"];
    self->reminderSwitch  = [decoder decodeIntForKey:@"14"];
    self->breakAlertsSwitch  = [decoder decodeIntForKey:@"15"];
    self->healthWarningsSwitch  = [decoder decodeIntForKey:@"16"];
    self->officeTimingsSwitch  = [decoder decodeIntForKey:@"17"];
    self->officestart = [decoder decodeObjectForKey:@"18"];
    self->officeend = [decoder decodeObjectForKey:@"19"];
    self->lunchstart = [decoder decodeObjectForKey:@"20"];
    self->lunchend = [decoder decodeObjectForKey:@"21"];
    self->myname = [decoder decodeObjectForKey:@"22"];
    self->cmpnyname = [decoder decodeObjectForKey:@"23"];
    self->imgpath = [decoder decodeObjectForKey:@"24"];
    self->install = [decoder decodeIntForKey:@"25"];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeInt:self->notificationswitch forKey:@"13"];
    [encoder encodeInt:self->reminderSwitch forKey:@"14"];
    [encoder encodeInt:self->breakAlertsSwitch forKey:@"15"];
    [encoder encodeInt:self->healthWarningsSwitch forKey:@"16"];
    [encoder encodeInt:self->officeTimingsSwitch forKey:@"17"];
    [encoder encodeObject:self->officestart forKey:@"18"];
    [encoder encodeObject:self->officeend forKey:@"19"];
    [encoder encodeObject:self->lunchstart forKey:@"20"];
    [encoder encodeObject:self->lunchend forKey:@"21"];
    [encoder encodeObject:self->myname forKey:@"22"];
    [encoder encodeObject:self->cmpnyname forKey:@"23"];
    [encoder encodeObject:self->imgpath forKey:@"24"];
    [encoder encodeInt:self->install forKey:@"25"];
}

-(SettingsControl *)initialize{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathUrl = [NSString stringWithFormat:@"settingsconfiguration.txt"];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pathUrl];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        SettingsControl *tempObject = [[SettingsControl alloc]init];
        tempObject = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        return tempObject;   
        
    }
    
    notificationswitch=true;
    reminderSwitch=true;
    breakAlertsSwitch=false;
    healthWarningsSwitch=true;
    officeTimingsSwitch=false;
    install=false;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *datestring=[dateFormat stringFromDate:[NSDate date]];
    [dateFormat setDateFormat:@"dd MMM yyyy HH:mm:ss " ];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    officestart=[dateFormat dateFromString:[NSString stringWithFormat:@"%@ 8:30:00 ",datestring]];
    officeend=[dateFormat dateFromString:[NSString stringWithFormat:@"%@ 17:30:00 ",datestring]];
    lunchstart=[dateFormat dateFromString:[NSString stringWithFormat:@"%@ 12:00:00 ",datestring]];
    lunchend=[dateFormat dateFromString:[NSString stringWithFormat:@"%@ 12:30:00 ",datestring]];
    [self saveToFile:self];

    return self;

}
-(void)saveToFile:(SettingsControl *)object{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathUrl = [NSString stringWithFormat:@"settingsconfiguration.txt"];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pathUrl];
    [NSKeyedArchiver archiveRootObject:object toFile:path];
    
    
}
@end