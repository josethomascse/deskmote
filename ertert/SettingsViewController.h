//
//  SettingsViewController.h
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISwitch *state_Changeswitch;
@property (strong, nonatomic) IBOutlet UISwitch *switch_Reminder;
@property (strong, nonatomic) IBOutlet UISwitch *switch_BreakAlert;
@property (strong, nonatomic) IBOutlet UISwitch *switch_Healthwarnings;
@property (strong, nonatomic) IBOutlet UISwitch *switch_OfficeTiming;
@property (weak, nonatomic) IBOutlet UIButton *officestart;
@property (weak, nonatomic) IBOutlet UIButton *officeend;
@property (weak, nonatomic) IBOutlet UIButton *lunchstart;
@property (weak, nonatomic) IBOutlet UIButton *lunchend;
@end
