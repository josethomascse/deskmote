//
//  SettingsViewController.m
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsControl.h"
#import "PickerViewController.h"
#import "Status.h"
#import "AppDelegate.h"
@interface SettingsViewController ()
{
    SettingsControl *file;
}
@end

@implementation SettingsViewController
- (void)viewDidLoad {

    [super viewDidLoad];
    [self loadswitches];
    [self loadtimes];

   
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadtimes)
                                                 name:@"reloadtime"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeNavBarColor) name:@"sate_change" object:nil];

}
-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:@"reloadtime"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sate_change" object:nil];

}
-(void)changeNavBarColor{
    Status *st=[[Status alloc]initialize];
    self.navigationController.navigationBar.barTintColor=[st retrieve_tintcolor];
}

-(void)loadtimes
{
    SettingsControl *scobj = [[SettingsControl alloc]init];
    scobj = [scobj initialize];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
    
    [_officestart setTitle: [dateFormatter stringFromDate:[scobj retrieveOfficeStart]] forState:UIControlStateNormal];
    [_officeend setTitle:[dateFormatter stringFromDate:[scobj retrieveOfficeEnd]] forState:UIControlStateNormal];
    [_lunchstart setTitle:[dateFormatter stringFromDate:[scobj retrieveLunchStart]] forState:UIControlStateNormal];
    [_lunchend setTitle:[dateFormatter stringFromDate:[scobj retrieveLunchEnd]] forState:UIControlStateNormal];

}
-(void)loadswitches
{
    SettingsControl *scobj = [[SettingsControl alloc]init];
    scobj = [scobj initialize];
    [_state_Changeswitch setOn:[scobj retrieveNotificationStatus]] ;
     [_switch_Reminder setOn:[scobj retrieveRemindersStatus]] ;
     [_switch_Healthwarnings setOn:[scobj retrieveHealthWarningsStatus]] ;
     [_switch_OfficeTiming setOn:[scobj retrieveOfficeTimingsStatus]] ;
     [_switch_BreakAlert setOn:[scobj retrieveBreakAlertsStatus]] ;



}

- (IBAction)switch_changed:(id)sender {
    file=[[SettingsControl alloc]init];
    file=[file initialize];
        NSInteger tag=[sender tag];
    [self switch_Change_recursivemethod:tag];
    [file saveToFile:file];
 
}
-(void)switch_Change_recursivemethod:(NSInteger) tag
{
    

    switch (tag) {
        case 0:
            if(_state_Changeswitch.isOn)
            {
        
                
                if (!_switch_BreakAlert.isOn)
                {
                    [_switch_BreakAlert setOn:YES animated:YES];
                    [self switch_Change_recursivemethod:2];
                }
                
                if (!_switch_Healthwarnings.isOn)
                {
                    [_switch_Healthwarnings setOn:YES animated:YES];
                    [self switch_Change_recursivemethod:3];
                }
             
                if (!_switch_OfficeTiming.isOn)
                {
                    [_switch_OfficeTiming setOn:YES animated:YES];
                    [self switch_Change_recursivemethod:4];
                }
                if (!_switch_Reminder.isOn)
                {
                    [_switch_Reminder setOn:YES animated:YES];
                    [self switch_Change_recursivemethod:1];
                }
               
            }
            else
            {
                if (_switch_Reminder.isOn)
                {
                    [_switch_Reminder setOn:NO animated:YES];
                    [self switch_Change_recursivemethod:1];
                }
                if (_switch_BreakAlert.isOn)
                {
                    [_switch_BreakAlert setOn:NO animated:YES];
                    [self switch_Change_recursivemethod:2];
                }
                if (_switch_Healthwarnings.isOn)
                {
                    [_switch_Healthwarnings setOn:NO animated:YES];
                    [self switch_Change_recursivemethod:3];
                }
                if (_switch_OfficeTiming.isOn)
                {
                    [_switch_OfficeTiming setOn:NO animated:YES];
                    [self switch_Change_recursivemethod:4];
                }

                   [file setnotificationswitch:false];
//                 
            }
            
            
            break;
            
        case 1:
            if( _switch_Reminder.isOn)
            {
                [file setreminderSwitch:true];
                [file setnotificationswitch:true];
                [_state_Changeswitch setOn:YES animated:YES];
//                 
            }
            else
            {


                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message !!" message:@"All your reminders will be deleted, Continue anyway:" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
                [alert show];
            }
            
            break;
            
        case 2:
            if (_switch_BreakAlert.isOn)
            {
                [file setbreakAlertsSwitch:true];
                
                // Schedule the notification
                [self cancel_allNotifications:@"breakalert1"];
                UILocalNotification *breakalerts1 = [[UILocalNotification alloc] init];
                NSDate *firedate=[self convertDate:[file retrieveLunchStart]];
                if ([[NSDate date] compare:firedate]==NSOrderedDescending)
                    breakalerts1.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
                else
                    breakalerts1.fireDate=firedate;
                breakalerts1.alertBody = @"Lunch Time";
                breakalerts1.alertAction = @"Show me the item";
                
                NSDictionary *info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert1"];
                breakalerts1.userInfo=info;
                breakalerts1.timeZone = [NSTimeZone defaultTimeZone];
                breakalerts1.repeatInterval = NSCalendarUnitDay;
                
               
                
//                breakalerts1.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                breakalerts1.soundName=UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts1];
                // [self setBadgenumber];
                
                
                
                // Schedule the notification
                [self cancel_allNotifications:@"breakalert2"];
                UILocalNotification *breakalerts2 = [[UILocalNotification alloc] init];
                firedate=[self convertDate:[file retrieveLunchEnd]];
                if ([[NSDate date] compare:firedate]==NSOrderedDescending)
                    breakalerts2.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
                else
                    breakalerts2.fireDate=firedate;
                breakalerts2.alertBody = @"Lunch Time Over";
                breakalerts2.alertAction = @"Show me the item";
                
                info=[NSDictionary dictionaryWithObject:@"Lunch break" forKey:@"breakalert2"];
                breakalerts2.userInfo=info;
                breakalerts2.timeZone = [NSTimeZone defaultTimeZone];
                breakalerts2.repeatInterval = NSCalendarUnitDay;
//                breakalerts2.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                breakalerts2.soundName=UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] scheduleLocalNotification:breakalerts2];
//                [self setBadgenumber];
        
                [file setnotificationswitch:true];
                [_state_Changeswitch setOn:YES animated:YES];
            }
            else//Cancel all scheduled breakAlerts
            {
   
                [file setbreakAlertsSwitch:false];
                [self cancel_allNotifications:@"breakalert1"];
                [self cancel_allNotifications:@"breakalert2"];
                [self check_everythingoff];
               
            }
            break;
        case 3:
            if(_switch_Healthwarnings.isOn)
            {
                [file sethealthWarningsSwitch:true];
                [file setnotificationswitch:true];
                [_state_Changeswitch setOn:YES animated:YES];

            }
            else
            {
                [self cancel_allNotifications:@"health_warning"];
                [file sethealthWarningsSwitch:false];
                [self check_everythingoff];
            }
    
            break;
            
        case 4:
            if (_switch_OfficeTiming.isOn)
            {
                [file setofficeTimingsSwitch:true];
                
                [self cancel_allNotifications:@"officeTimings1"];
                // For in Time
              
                UILocalNotification *officeTimings1= [[UILocalNotification alloc] init];
               NSDate *firedate=[self convertDate:[file retrieveOfficeStart]];
                if ([[NSDate date] compare:firedate]==NSOrderedDescending)
                    officeTimings1.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
                else
                    officeTimings1.fireDate=firedate;
                
                officeTimings1.alertBody = @"Office Time!";
                officeTimings1.alertAction = @"Show me the item";
                NSDictionary *info=[NSDictionary dictionaryWithObject:@"office start alert" forKey:@"officeTimings1"];
                officeTimings1.userInfo=info;
                officeTimings1.timeZone = [NSTimeZone defaultTimeZone];
                officeTimings1.repeatInterval=NSCalendarUnitDay;
                officeTimings1.soundName=UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings1];
              //  [self setBadgenumber];
                
                
                
                [self cancel_allNotifications:@"officeTimings2"];
                UILocalNotification *officeTimings2= [[UILocalNotification alloc] init];
               
                firedate=[self convertDate:[file retrieveOfficeEnd]];
                if ([[NSDate date] compare:firedate]==NSOrderedDescending)
                    officeTimings2.fireDate=[NSDate dateWithTimeInterval:3600*24 sinceDate:firedate];
                else
                    officeTimings2.fireDate=firedate;
                
                officeTimings2.alertBody = @"Go Home!";
                officeTimings2.alertAction = @"Show me the item";
                officeTimings1.repeatInterval=NSCalendarUnitDay;
                info=[NSDictionary dictionaryWithObject:@"office stop alert" forKey:@"officeTimings2"];
                officeTimings2.userInfo=info;
                officeTimings2.timeZone = [NSTimeZone defaultTimeZone];
                officeTimings2.soundName=UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings2];
//                      [self setBadgenumber];
                
//                [file setnotificationswitch:true];
                [_state_Changeswitch setOn:YES animated:YES];
                 [file setnotificationswitch:true];

            }
            else
            {
              [file setofficeTimingsSwitch:false];
              [self cancel_allNotifications:@"officeTimings1"];
                 [self cancel_allNotifications:@"officeTimings2"];
                     [self check_everythingoff];
            }
            break;
        default:
            break;
    }


}


- (IBAction)showtimepicker:(id)sender {
    
    PickerViewController *popController = [[PickerViewController alloc] init];
    popController.sourceView = sender;
    popController.contentSize = CGSizeMake(195, 45);
    popController.arrowDirection = UIPopoverArrowDirectionDown;
    popController.sendertag=[sender tag];
    [self presentViewController:popController
                       animated:YES
                     completion:nil];
    

}

//alert delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    file=[[SettingsControl alloc]init];
    file=[file initialize];


    
    switch (buttonIndex) {
        case 0:
        {
            [self cancel_allNotifications:@"reminder"];
              [file setreminderSwitch:false];
                 [self check_everythingoff];
            
        }
            break;
            
        case 1:
            [_switch_Reminder setOn:YES animated:YES];
            [file setreminderSwitch:true];
             [file setnotificationswitch:true];
            [self switch_Change_recursivemethod:1];
        break;
            
        default:
            break;
    }
        [file saveToFile:file];
}
-(void)check_everythingoff
{
    if (_switch_BreakAlert.isOn || _switch_Healthwarnings.isOn  || _switch_OfficeTiming.isOn  || _switch_Reminder.isOn)      {
            return;
        }
        [ _state_Changeswitch setOn:NO animated:YES];
        [file setnotificationswitch:false];
        [file saveToFile:file];

}

-(void)cancel_allNotifications:(NSString *)identifier
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification *localNotification = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:identifier]];
        if(![uid isEqualToString:@"(null)"])
        {
            
            [app cancelLocalNotification:eventArray[i]];
        }
    }
//    [self setBadgenumber];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
-(NSDate *)convertDate:(NSDate *)date
{
    //date string
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MM yyyy"];
    NSString *datestring=[dateFormat stringFromDate:[NSDate date]];
    
    //time string
    [dateFormat setDateFormat:@"hh:mm:ss a"];
    NSString *timestring=[dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"dd MM yyyy hh:mm:ss a"];
   
    // [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
   return [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",datestring,timestring]];
}

//-(void)setBadgenumber
//{
//    int badgeNumber=1,flag;
//    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
//    NSInteger count=[localNotifications count];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
//    
//    
//    for (int i=0; i<count; i++) {
//        flag=1;
//        localNotification = [localNotifications objectAtIndex:i];
//        NSDictionary *userInfoCurrent = localNotification.userInfo;
//        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        if (1==flag) {
//            localNotification.applicationIconBadgeNumber=badgeNumber;
//            badgeNumber=badgeNumber+1;
//        }
//        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    }
//    
//}

@end
