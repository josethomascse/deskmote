//
//  Statistics.h
//  deskmote
//
//  Created by user on 7/3/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Statistics : NSManagedObject

@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSNumber * deskTime;
@property (nonatomic, retain) NSNumber * insideTime;
@property (nonatomic, retain) NSNumber * outsideTime;

@end
