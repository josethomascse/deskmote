//
//  Statistics.m
//  deskmote
//
//  Created by user on 7/3/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "Statistics.h"


@implementation Statistics

@dynamic date;
@dynamic deskTime;
@dynamic insideTime;
@dynamic outsideTime;

@end
