//
//  StatisticsOperations.h
//  
//
//  Created by user on 6/24/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Statistics.h"

struct time{
    int hours;
    int minutes;
    int seconds;
};

struct statObject{
    BOOL valid;
    struct time inside;
    struct time outside;
    struct time desk;
};

@interface StatisticsOperations : NSObject

-(void)appendOutsideOfficeTime:(long int)tempTime date:(NSDate *)tempDate;
-(void)appendInsideOfficeTime:(long int)tempTime date:(NSDate *)tempDate;
-(void)appendAtWorkTime:(long int)tempTime date:(NSDate *)tempDate;
-(struct statObject)retrieveTimeObject:(NSDate*)tempDate seconds:(BOOL)secondsDec realTime:(BOOL)realTimeDec;
-(void)createDateEntry:(NSDate *)tempDate;

@end
