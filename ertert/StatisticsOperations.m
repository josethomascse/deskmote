//
//  StatisticsOperations.m
//  
//
//  Created by user on 6/24/15.
//
//

#import "StatisticsOperations.h"
#import <UIKit/UIKit.h>
#import "Statistics.h"
#import "Status.h"
#import "SettingsControl.h"
@interface StatisticsOperations()
@property (strong) NSMutableArray *object;
@end

@implementation StatisticsOperations

-(NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

//function to increment time inside office
-(void)appendInsideOfficeTime:(long int)tempTime date:(NSDate *)tempDate{
    //fetch time object with passed date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *timestring = [dateFormatter stringFromDate: tempDate];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Statistics"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Statistics"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date LIKE[c] %@",timestring];
    [fetchRequest setPredicate:predicate];
    self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if(self.object.count==0){
        //creating database entry
        [self createDateEntry:tempDate];
        managedObjectContext = [self managedObjectContext];
        self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        }
    //appending time supplied
    Statistics *record = [self.object objectAtIndex:0];
    record.insideTime=[NSNumber numberWithInteger:[record.insideTime intValue]+tempTime];
    [managedObjectContext save:nil];
}

-(void)appendOutsideOfficeTime:(long int)tempTime date:(NSDate *)tempDate{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *timestring = [dateFormatter stringFromDate: tempDate];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Statistics"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Statistics"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date LIKE[c] %@",timestring];
    [fetchRequest setPredicate:predicate];
    self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if(self.object.count==0){
        [self createDateEntry:tempDate];
        managedObjectContext = [self managedObjectContext];
        self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
         }
    Statistics *record = [self.object objectAtIndex:0];
    record.outsideTime=[NSNumber numberWithInteger:[record.outsideTime intValue]+tempTime];    [managedObjectContext save:nil];
}

-(void)appendAtWorkTime:(long int)tempTime date:(NSDate *)tempDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *timestring = [dateFormatter stringFromDate: tempDate];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Statistics"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Statistics"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date LIKE[c] %@",timestring];
    [fetchRequest setPredicate:predicate];
    self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if(self.object.count==0){
        [self createDateEntry:tempDate];
        managedObjectContext = [self managedObjectContext];
        self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        }
    Statistics *record = [self.object objectAtIndex:0];
    record.deskTime=[NSNumber numberWithInteger:[record.deskTime intValue]+tempTime];
    [managedObjectContext save:nil];
}

-(void)createDateEntry:(NSDate *)tempDate{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Statistics" inManagedObjectContext:context];
    Statistics *record=[[Statistics alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *timestring = [dateFormatter stringFromDate: tempDate];
    record.date=timestring;
    record.insideTime=[NSNumber numberWithInt:0];
    record.outsideTime=[NSNumber numberWithInt:0];
    record.deskTime=[NSNumber numberWithInt:0];
    NSError *error = nil;
   [context save:&error];
}

-(struct statObject)retrieveTimeObject:(NSDate*)tempDate seconds:(BOOL)secondsDec realTime:(BOOL)realTimeDec{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *timestring = [dateFormatter stringFromDate: tempDate];
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Statistics"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Statistics"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date LIKE[c] %@",timestring];
    [fetchRequest setPredicate:predicate];
    self.object = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    struct statObject stat;
    stat.inside.minutes=0;
    stat.inside.hours=0;
    stat.outside.hours=0;
    stat.outside.minutes=0;
    stat.desk.hours=0;
    stat.desk.minutes=0;

    if(self.object.count!=0){
        Statistics *record = [self.object objectAtIndex:0];
        stat.valid=true;
        stat.inside.seconds=[record.insideTime intValue];
        stat.outside.seconds=[record.outsideTime intValue];
        stat.desk.seconds=[record.deskTime intValue];
        if(secondsDec == 1)
            return stat;
    }else{
        stat.valid=false;
        stat.inside.seconds=0;
        stat.outside.seconds=0;
        stat.desk.seconds=0;
        if(secondsDec==1)
            return stat;
    }
    
    if(realTimeDec){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd, yyyy"];
        NSString *stringFromDate = [formatter stringFromDate:tempDate];
        NSString *stringFromDate2 =[formatter stringFromDate:[NSDate date]];
        if([stringFromDate isEqualToString:stringFromDate2]){
            Status *state=[[Status alloc]init];
            state=[state initialize];
            SettingsControl *st=[[SettingsControl alloc]init];
            st=[st initialize];
            if(![st retrieveDbStatus]){
                int tempTime;
                tempTime=[state getCurrentStartDuration];
                if(!stat.valid)
                    stat.valid=true;
                if ([state retrieveCurrentStatus]==0)
                    stat.inside.seconds+=tempTime;
                else if([state retrieveCurrentStatus]==1)
                    stat.outside.seconds+=tempTime;
                else
                    stat.desk.seconds+=tempTime;
            }
        }
    }
    
    stat.inside.minutes=stat.inside.seconds/60;
    stat.inside.hours=stat.inside.minutes/60;
    stat.inside.minutes=stat.inside.minutes%60;
    stat.inside.seconds=stat.inside.seconds%60;
    stat.outside.minutes=stat.outside.seconds/60;
    stat.outside.hours=stat.outside.minutes/60;
    stat.outside.minutes=stat.outside.minutes%60;
    stat.outside.seconds=stat.outside.seconds%60;
    stat.desk.minutes=stat.desk.seconds/60;
    stat.desk.hours=stat.desk.minutes/60;
    stat.desk.minutes=stat.desk.minutes%60;
    stat.desk.seconds=stat.desk.seconds%60;
    return stat;
}
@end

