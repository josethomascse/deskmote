//
//  StatisticsViewController.h
//  deskmote
//
//  Created by user on 7/1/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatisticsViewController : UIViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIView *actualView;
@property (strong, nonatomic) IBOutlet UIButton *dayButton;
@property (strong, nonatomic) IBOutlet UIButton *weekButton;
@property (strong, nonatomic) IBOutlet UIButton *monthButton;
@property (weak, nonatomic) IBOutlet UIView *bezierview;

@end
