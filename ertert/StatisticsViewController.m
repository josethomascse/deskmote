//
//  StatisticsViewController.m
//  deskmote
//
//  Created by user on 7/1/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "StatisticsViewController.h"
#import "StatisticsOperations.h"
#import "Status.h"
#import "DLPieChart.h"
#import "AppDelegate.h"
@interface StatisticsViewController ()
@property DLPieChart *piechartview;
@end

@implementation StatisticsViewController{
    UIScrollView *pieChartView;
    UIScrollView *barChartWeekView;
    UIScrollView *barChartMonthView;
}

CGRect screenRect;
CGFloat screenWidth;
CGFloat screenHeight;

NSInteger weekend,weekstart;
NSDate *datefrom1,*datefrom2;
int selectedIndex=0;
int weekNum=0;
CAShapeLayer *shapeLayer,*shapeLayer2;
NSInteger currentmonth,currentyear,x1,x2;

- (void)viewDidLoad {
    
    AppDelegate *appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    
      [self.view addSubview:appDelegate.navBar];
    [self.view addSubview:appDelegate.slidingMenu];
    appDelegate.navBar.titleLabel.text=@"Statistics";
    [self.view addSubview:appDelegate.veilView];
    
    [self addChildViewController:appDelegate.table];
    [appDelegate.table didMoveToParentViewController:self];
    
    [super viewDidLoad];
    
    screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    
    [self PieChartShow:self];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    x1=0.0;
    x2=screenWidth;
    [path1 moveToPoint:CGPointMake(x1,0)];
    [path1 addLineToPoint:CGPointMake(x2,0)];
    shapeLayer2 = [CAShapeLayer layer];
    shapeLayer2.path = [path1 CGPath];
    shapeLayer2.strokeColor = [[UIColor lightGrayColor] CGColor];
    shapeLayer2.lineWidth = 0.4;
    shapeLayer2.fillColor = [[UIColor lightGrayColor] CGColor];
    [self.bezierview.layer addSublayer:shapeLayer2];
    
    [shapeLayer removeFromSuperlayer];
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    x1=(0.17*screenWidth)-20.0;
    x2=(0.17*screenWidth)+20.0;
    [path2 moveToPoint:CGPointMake(x1, 0)];
    [path2 addLineToPoint:CGPointMake(x2,0)];
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path2 CGPath];
    shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor darkGrayColor] CGColor];
    [self.bezierview.layer addSublayer:shapeLayer];
   
}
-(void)changeNavBarColor{
    Status *st=[[Status alloc]initialize];
    self.navigationController.navigationBar.barTintColor=[st retrieve_tintcolor];
}
-(void)viewDidAppear:(BOOL)animated
{
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeNavBarColor) name:@"sate_change" object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sate_change" object:nil];
}

- (IBAction)PieChartShow:(id)sender {
    weekNum=0;
    selectedIndex=0;
    [shapeLayer removeFromSuperlayer];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    x1=(0.17*screenWidth)-20.0;
    x2=(0.17*screenWidth)+20.0;
    
    [path1 moveToPoint:CGPointMake(x1, 0)];
    [path1 addLineToPoint:CGPointMake(x2,0)];
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path1 CGPath];
    shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor darkGrayColor] CGColor];
    [self.bezierview.layer addSublayer:shapeLayer];
    
     NSDate *dateNow=[NSDate date];
     NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
     [myDateFormatter setDateFormat:@"MMM dd, yyyy"];
     NSString *stringFromDate = [[myDateFormatter stringFromDate:dateNow]uppercaseString];
     _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
    
     [_dayButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
     [_weekButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
     [_monthButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
     [self StatisticsForDay:[NSDate date]];
}

- (IBAction)BarChartWeekShow:(id)sender {
    weekNum=0;
    selectedIndex=1;
    [shapeLayer removeFromSuperlayer];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    x1=(0.5*screenWidth)-20.0;
    x2=(0.5*screenWidth)+20.0;
    [path1 moveToPoint:CGPointMake(x1, 0)];
    [path1 addLineToPoint:CGPointMake(x2,0)];
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path1 CGPath];
    shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor darkGrayColor] CGColor];
    [self.bezierview.layer addSublayer:shapeLayer];

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd,yyyy"];
    NSDate *now = [[NSDate alloc] init];
    
    // get the weekday of the current date
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSCalendarUnitWeekday fromDate:now];
    NSInteger weekday = [components weekday];
    
    switch (weekday) {
        case 1:
            weekstart=0;
            weekend=6;
            break;
        case 2:
            weekstart=-1;
            weekend = 5;
            break;
        case 3:
            weekstart=-2;
            weekend = 4;
            break;
        case 4:
            weekstart=-3;
            weekend = 3;
            break;
        case 5:
            weekstart=-4;
            weekend = 2;
            break;
        case 6:
            weekstart=-5;
            weekend = 1;
            break;
        case 7:
            weekstart=-6;
            weekend=0;
        default:
            break;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    
    NSDateComponents* component = [[NSDateComponents alloc]init];
    [component setDay:weekstart];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    datefrom1= [calendar dateByAddingComponents:component toDate:now options:0];
    
    NSString *stringFromDate1 = [dateFormatter stringFromDate:datefrom1];
    
    NSString *from = [NSString stringWithFormat:@"%@",stringFromDate1];
    
    [component setDay:weekend];
    datefrom2= [calendar dateByAddingComponents:component toDate:now options:0];
    NSString *stringFromDate2 = [dateFormat stringFromDate:datefrom2];
    NSString *to = [NSString stringWithFormat:@"%@",stringFromDate2];
    
    _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",from,to];
    
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"MMM dd, yyyy"];
//     NSDate *date=[NSDate date];
//     NSString *todayDate = [[dateFormat stringFromDate:date]uppercaseString];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"MMM dd"];
//    NSDateComponents* component = [[NSDateComponents alloc]init];
//    [component setDay:-7];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    date= [calendar dateByAddingComponents:component toDate:date options:0];
//    NSString *prevDate = [[dateFormatter stringFromDate:date]uppercaseString];
//    _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",prevDate,todayDate];
    
    [_dayButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_weekButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_monthButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [self StatisticsForWeek:datefrom2];
}

- (IBAction)BarChartMonthShow:(id)sender {
    weekNum=0;
    selectedIndex=2;
    [shapeLayer removeFromSuperlayer];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    x1=(0.83*screenWidth)-24.0;
    x2=(0.83*screenWidth)+24.0;
    [path1 moveToPoint:CGPointMake(x1, 0)];
    [path1 addLineToPoint:CGPointMake(x2,0)];
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path1 CGPath];
    shapeLayer.strokeColor = [[UIColor darkGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor darkGrayColor] CGColor];
    [self.bezierview.layer addSublayer:shapeLayer];

    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM, yyyy"];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitMonth|NSCalendarUnitYear fromDate:currentDate];
    currentmonth = [components month]; //gives you month
    currentyear = [components year];
    NSString * dateString = [NSString stringWithFormat: @"%ld,%ld", (long)currentmonth,(long)currentyear];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM,yyyy"];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM, yyyy"];
    NSString *stringFromDate = [[formatter stringFromDate:myDate]uppercaseString];
    _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
    
    [_dayButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_weekButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_monthButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [self StatisticsForMonth:myDate];
}

-(void)StatisticsForDay:(NSDate*)date{

    [[_actualView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    pieChartView=[[UIScrollView alloc]init];
    pieChartView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    pieChartView.accessibilityActivationPoint = CGPointMake(100, 100);
    pieChartView.delegate = self;

    struct statObject stat;
    StatisticsOperations *st=[[StatisticsOperations alloc]init];
    int hour,min;
    stat=[st retrieveTimeObject:date seconds:0 realTime:1];
    if(!stat.valid)
            return;
    hour=stat.inside.hours+stat.outside.hours+stat.desk.hours;
    min=stat.outside.minutes+stat.inside.minutes+stat.desk.minutes;
    hour+=min/60;
    min=min%60;
    
    self.piechartview=[[DLPieChart alloc]initWithFrame:CGRectMake((0.14*screenWidth),0,(0.72*screenWidth), (0.72*screenWidth))];
    
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    [dataArray addObject:[NSString stringWithFormat:@"%d",(stat.outside.hours*60)+stat.outside.minutes]];
    [dataArray addObject:[NSString stringWithFormat:@"%d",(stat.inside.hours*60)+stat.inside.minutes]];
    [dataArray addObject:[NSString stringWithFormat:@"%d",(stat.desk.hours*60)+stat.desk.minutes]];
    [self.piechartview renderInLayer:self.piechartview dataArray:dataArray];
    
    UIView *doughnut=[[UIView alloc]initWithFrame:CGRectMake((0.3*screenWidth), (0.16*screenWidth), (0.4*screenWidth),(0.4*screenWidth))];
    doughnut.layer.cornerRadius = doughnut.frame.size.height /2;
    doughnut.layer.masksToBounds = YES;
    doughnut.layer.borderWidth = 0;
    doughnut.backgroundColor=[UIColor whiteColor];
    [pieChartView addSubview:self.piechartview];
    [pieChartView addSubview:doughnut];
    
    UILabel *total=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80,30 )];
    total.center=CGPointMake(doughnut.center.x, doughnut.center.y-10);
    total.text=[NSString stringWithFormat:@"TOTAL"];
    total.textColor=[UIColor darkGrayColor];
    total.textAlignment = NSTextAlignmentCenter;
    [pieChartView addSubview:total];
    
    UILabel *tot=[[UILabel alloc]initWithFrame:CGRectMake(0,0, 80,30 )];
    tot.center=CGPointMake(doughnut.center.x, doughnut.center.y+20);
    tot.text=[NSString stringWithFormat:@"%02d:%02d",hour,min];
    tot.font = [UIFont fontWithName:@"Digital-7" size:35];
    tot.textColor=[UIColor darkGrayColor];
    tot.textAlignment = NSTextAlignmentCenter;
    [pieChartView addSubview:tot];
    
    UIView *blueRound=[[UIView alloc]initWithFrame:CGRectMake((0.1*screenWidth), (0.48*screenHeight), 18, 18)];
    blueRound.layer.cornerRadius = blueRound.frame.size.height /2;
    blueRound.layer.masksToBounds = YES;
    blueRound.layer.borderWidth = 0;
    blueRound.backgroundColor=[UIColor colorWithRed:69.0/255.0 green:176.0/255.0 blue:207.0/255.0 alpha:1];
    [pieChartView addSubview:blueRound];
    
    UIView *greenRound=[[UIView alloc]initWithFrame:CGRectMake((0.1*screenWidth), (0.58*screenHeight), 18, 18)];
    greenRound.layer.cornerRadius = greenRound.frame.size.height /2;
    greenRound.layer.masksToBounds = YES;
    greenRound.layer.borderWidth = 0;
    greenRound.backgroundColor=[UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:63.0/255.0 alpha:1];
    [pieChartView addSubview:greenRound];
    
    UIView *yellowRound=[[UIView alloc]initWithFrame:CGRectMake((0.1*screenWidth), (0.68*screenHeight), 18, 18)];
    yellowRound.layer.cornerRadius = yellowRound.frame.size.height /2;
    yellowRound.layer.masksToBounds = YES;
    yellowRound.layer.borderWidth = 0;
    yellowRound.backgroundColor=[UIColor colorWithRed:241.0/255.0 green:182.0/255.0 blue:13.0/255.0 alpha:1];
    [pieChartView addSubview:yellowRound];
    
    UILabel *inside_text=[[UILabel alloc]initWithFrame:CGRectMake((0.24*screenWidth), (0.47*screenHeight), 150,30 )];
    inside_text.text=[NSString stringWithFormat:@"AT YOUR DESK"];
    inside_text.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:15];
    inside_text.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:inside_text];
    
    UILabel *outside_text=[[UILabel alloc]initWithFrame:CGRectMake((0.24*screenWidth), (0.57*screenHeight), 150,30 )];
    outside_text.text=[NSString stringWithFormat:@"INSIDE OFFICE"];
    outside_text.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:15];
    outside_text.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:outside_text];
    
    UILabel *desk_text=[[UILabel alloc]initWithFrame:CGRectMake((0.24*screenWidth), (0.67*screenHeight), 150,30 )];
    desk_text.text=[NSString stringWithFormat:@"OUTSIDE OFFICE"];
    desk_text.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:15];
    desk_text.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:desk_text];
    
    UILabel *desktime=[[UILabel alloc]initWithFrame:CGRectMake((0.75*screenWidth), (0.47*screenHeight), 80,30 )];
    desktime.text=[NSString stringWithFormat:@"%02d:%02d",stat.desk.hours,stat.desk.minutes];
    desktime.font = [UIFont fontWithName:@"Digital-7" size:25];
    desktime.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:desktime];
    
    UILabel *insidetime=[[UILabel alloc]initWithFrame:CGRectMake((0.75*screenWidth), (0.57*screenHeight), 80,30 )];
    insidetime.text=[NSString stringWithFormat:@"%02d:%02d",stat.inside.hours,stat.inside.minutes];
    insidetime.font = [UIFont fontWithName:@"Digital-7" size:25];
    insidetime.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:insidetime];
    
    UILabel *outsidetime=[[UILabel alloc]initWithFrame:CGRectMake((0.75*screenWidth), (0.67*screenHeight), 80,30 )];
    outsidetime.text=[NSString stringWithFormat:@"%02d:%02d",stat.outside.hours,stat.outside.minutes];
    outsidetime.font = [UIFont fontWithName:@"Digital-7" size:25];
    outsidetime.textColor=[UIColor darkGrayColor];
    [pieChartView addSubview:outsidetime];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    x1=10.0;
    x2=screenWidth-10.0;
    [path1 moveToPoint:CGPointMake(x1, (0.55*screenHeight))];
    [path1 addLineToPoint:CGPointMake(x2, (0.55*screenHeight))];
    shapeLayer2 = [CAShapeLayer layer];
    shapeLayer2.path = [path1 CGPath];
    shapeLayer2.strokeColor = [[UIColor lightGrayColor] CGColor];
    shapeLayer2.lineWidth = 0.4;
    shapeLayer2.fillColor = [[UIColor lightGrayColor] CGColor];
    [pieChartView.layer addSublayer:shapeLayer2];
    
    path1 = [UIBezierPath bezierPath];
    x1=10.0;
    x2=screenWidth-10.0;
    [path1 moveToPoint:CGPointMake(x1, (0.65*screenHeight))];
    [path1 addLineToPoint:CGPointMake(x2, (0.65*screenHeight))];
    shapeLayer2 = [CAShapeLayer layer];
    shapeLayer2.path = [path1 CGPath];
    shapeLayer2.strokeColor = [[UIColor lightGrayColor] CGColor];
    shapeLayer2.lineWidth = 0.4;
    shapeLayer2.fillColor = [[UIColor lightGrayColor] CGColor];
    [pieChartView.layer addSublayer:shapeLayer2];
    [self.actualView addSubview:pieChartView];
    
    if(screenHeight<500)
        pieChartView.contentSize = CGSizeMake(screenWidth,screenHeight+60);
}

- (IBAction)nextButton:(id)sender {
    if(selectedIndex==0)
        [self nextdate];
    else if(selectedIndex==1)
        [self nextweek];
    else
        [self nextmonth];
}
- (IBAction)prevButton:(id)sender {
    if(selectedIndex==0)
        [self prevdate];
    else if(selectedIndex==1)
        [self prevweek];
    else
        [self prevmonth];
}

-(void)StatisticsForMonth:(NSDate *)date{
    [[_actualView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    barChartMonthView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    barChartMonthView.accessibilityActivationPoint = CGPointMake(100, 100);
    barChartMonthView.delegate = self;
    barChartMonthView.contentSize = CGSizeMake(screenWidth,
                                          screenHeight+60);

    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange rng = [cal rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    NSUInteger count = rng.length;
    struct statObject stat;
    int validNo=0;
    StatisticsOperations *s=[[StatisticsOperations alloc]init];
    for(int i=0;i<count;i++){
        stat=[s retrieveTimeObject:date seconds:0 realTime:1];
        if(stat.valid){
            [self drawBarGraphForMonth:stat index:validNo dayindex:i+1];
            validNo++;
        }
        NSDateComponents *components= [[NSDateComponents alloc] init];
        [components setDay:+1];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        date= [calendar dateByAddingComponents:components toDate:date options:0];
    }
    barChartMonthView.contentSize = CGSizeMake(screenWidth,
                                               (175+validNo*50));
    [_actualView addSubview:barChartMonthView];
}
- (IBAction)nextdate{
    NSString *date=_dateLabel.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateIncremented= [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString *stringFromDate = [[myDateFormatter stringFromDate:dateIncremented]uppercaseString];
    _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
    //    NSLog(@"%@", stringFromDate);
    [self StatisticsForDay:dateIncremented];
    
}
- (IBAction)prevdate {
    NSString *date=_dateLabel.text;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    
    
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:-1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateIncremented= [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString *stringFromDate = [[myDateFormatter stringFromDate:dateIncremented]uppercaseString];
    _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
      [self StatisticsForDay:dateIncremented];
}
- (IBAction)nextweek{
   
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"MMM dd"];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
//    NSDateComponents* component = [[NSDateComponents alloc]init];
//    [component setDay:(weekNum*7)+1];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDate *date = [calendar dateByAddingComponents:component toDate:[NSDate date] options:0];
//    NSString *endDate = [[dateFormat stringFromDate:date]uppercaseString];
//    [component setDay:((weekNum+1)*7)];
//    calendar = [NSCalendar currentCalendar];
//    date = [calendar dateByAddingComponents:component toDate:[NSDate date] options:0];
//    NSString *startDate = [[dateFormatter stringFromDate:date]uppercaseString];
//     _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",endDate,startDate];
//    [self StatisticsForWeek:date];
//     weekNum++;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd,yyyy"];
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    
    NSDateComponents* component = [[NSDateComponents alloc]init];
    [component setDay:1];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    datefrom1= [calendar dateByAddingComponents:component toDate:datefrom2 options:0];
    
    NSString *stringFromDate1 = [dateFormatter stringFromDate:datefrom1];
    
    NSString *from = [NSString stringWithFormat:@"%@",stringFromDate1];
    
    [component setDay:7];
    datefrom2= [calendar dateByAddingComponents:component toDate:datefrom2 options:0];
    NSString *stringFromDate2 = [dateFormat stringFromDate:datefrom2];
    NSString *to = [NSString stringWithFormat:@"%@",stringFromDate2];
    
    _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",from,to];
    [self StatisticsForWeek:datefrom2];

}
- (IBAction)prevweek{
//    weekNum--;
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"MMM dd"];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
//    NSDateComponents* component = [[NSDateComponents alloc]init];
//    [component setDay:((weekNum-1)*7)+1];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDate *date = [calendar dateByAddingComponents:component toDate:[NSDate date] options:0];
//    NSString *endDate = [[dateFormat stringFromDate:date]uppercaseString];
//    [component setDay:(weekNum*7)];
//    calendar = [NSCalendar currentCalendar];
//    date = [calendar dateByAddingComponents:component toDate:[NSDate date] options:0];
//    NSString *startDate = [[dateFormatter stringFromDate:date]uppercaseString];
//    _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",endDate,startDate];
//    
//      [self StatisticsForWeek:date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd,yyyy"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    
    NSDateComponents* component = [[NSDateComponents alloc]init];
   
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [component setDay:-1];
    datefrom2= [calendar dateByAddingComponents:component toDate:datefrom1 options:0];
    NSString *stringFromDate2 = [dateFormat stringFromDate:datefrom2];
    NSString *to = [NSString stringWithFormat:@"%@",stringFromDate2];
    
    [component setDay:-7];
    
    
    datefrom1= [calendar dateByAddingComponents:component toDate:datefrom1 options:0];
    
    NSString *stringFromDate1 = [dateFormatter stringFromDate:datefrom1];
    
    NSString *from = [NSString stringWithFormat:@"%@",stringFromDate1];
    
    _dateLabel.text = [NSString stringWithFormat:@"%@ - %@",from,to];
    
    [self StatisticsForWeek:datefrom2];

}
- (IBAction)nextmonth{
    
    
    if(currentmonth==12)
    {
        currentmonth=1;
        currentyear+=1;
    }
    else{
        
        currentmonth+= 1;
    }
    
    
    NSString * dateString = [NSString stringWithFormat: @"%ld,%ld", (long)currentmonth,(long)currentyear];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM,yyyy"];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM, yyyy"];
    NSString *stringFromDate = [[formatter stringFromDate:myDate]uppercaseString];
    _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
    [self StatisticsForMonth:myDate];
    
    
}

- (IBAction)prevmonth{
    if(currentmonth==1)
    {
        currentmonth=12;
        currentyear-=1;
    }
    else{
        
        currentmonth-= 1;
    }
    
    
    NSString * dateString = [NSString stringWithFormat: @"%ld,%ld", (long)currentmonth,(long)currentyear];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM,yyyy"];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM, yyyy"];
    NSString *stringFromDate = [[formatter stringFromDate:myDate]uppercaseString];
    _dateLabel.text = [NSString stringWithFormat:@"%@",stringFromDate];
     [self StatisticsForMonth:myDate];
}



-(void)StatisticsForWeek:(NSDate*)date{
    [[_actualView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    barChartWeekView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    barChartWeekView.accessibilityActivationPoint = CGPointMake(100, 100);
    barChartWeekView.delegate = self;
    barChartWeekView.contentSize = CGSizeMake(screenWidth,
                                              screenHeight+60);
    struct statObject stat;
    StatisticsOperations *s=[[StatisticsOperations alloc]init];
    NSDateComponents *components= [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    int validNo=0;
    [components setDay:-6];
    date= [calendar dateByAddingComponents:components toDate:date options:0];
    for(int i=6;i>=0;i--){
        NSDateComponents* components2 = [calendar components:NSCalendarUnitDay fromDate:date];
        stat=[s retrieveTimeObject:date seconds:0 realTime:1];
        if(stat.valid){
            [self drawBarGraphForWeek:stat index:validNo dayindex:(int)[components2 day]];
            validNo++;
        }
        [components setDay:+1];
        date= [calendar dateByAddingComponents:components toDate:date options:0];
    }
    barChartWeekView.contentSize = CGSizeMake(screenWidth,
                                               validNo*105);
    [_actualView addSubview:barChartWeekView];
}

-(void)drawBarGraphForMonth:(struct statObject)stat index:(int)row dayindex:(int)day{
    int total=(stat.inside.hours*60)+stat.inside.minutes+(stat.outside.hours*60)+stat.outside.minutes+(stat.desk.hours*60)+stat.desk.minutes;
    float insideper=0,outsideper=0,deskper=0;
    if(total!=0){
    insideper=(float)((stat.inside.hours*60)+stat.inside.minutes)/total;
    outsideper=(float)((stat.outside.hours*60)+stat.outside.minutes)/total;
    deskper=(float)((stat.desk.hours*60)+stat.desk.minutes)/total;
    }
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float barlength=(screenWidth-90);
    UILabel *dayLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 20+(row*50), 30, 10)];
    dayLabel.text=[NSString stringWithFormat:@"%02d",day];
    dayLabel.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    dayLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14];
    [barChartMonthView addSubview:dayLabel];
    UIView *myBox  = [[UIView alloc] initWithFrame:CGRectMake(55, 20+(row*50), barlength*deskper, 10)];
    myBox.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:176.0/255.0 blue:207.0/255.0 alpha:1];
    [barChartMonthView addSubview:myBox];
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(55+barlength*deskper, 20+(row*50), barlength*insideper, 10)];
    myBox.backgroundColor =[UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:63.0/255.0 alpha:1];
    [barChartMonthView addSubview:myBox];
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(55+barlength*insideper+barlength*deskper, 20+(row*50), barlength*outsideper, 10)];
    myBox.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:182.0/255.0 blue:13.0/255.0 alpha:1];
    [barChartMonthView addSubview:myBox];

    
}
-(void)drawBarGraphForWeek:(struct statObject)stat index:(int)row dayindex:(int)day{
//    int total=(stat.inside.hours*60)+stat.inside.minutes+(stat.outside.hours*60)+stat.outside.minutes+(stat.desk.hours*60)+stat.desk.minutes;
    int inside=(stat.inside.hours*60)+stat.inside.minutes;
    int outside=(stat.outside.hours*60)+stat.outside.minutes;
    int desk=(stat.desk.hours*60)+stat.desk.minutes;
    int greater=inside;
    if(greater<outside)
        greater=outside;
    if(greater<desk)
        greater=desk;
    float insideper=0,outsideper=0,deskper=0;
//    if(total!=0){
//        insideper=(float)((stat.inside.hours*60)+stat.inside.minutes)/total;
//        outsideper=(float)((stat.outside.hours*60)+stat.outside.minutes)/total;
//        deskper=(float)((stat.desk.hours*60)+stat.desk.minutes)/total;
//}
    if(greater!=0){
                insideper=(float)((stat.inside.hours*60)+stat.inside.minutes)/greater;
                outsideper=(float)((stat.outside.hours*60)+stat.outside.minutes)/greater;
                deskper=(float)((stat.desk.hours*60)+stat.desk.minutes)/greater;
        }
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float barlength=(screenWidth-120);
    UILabel *dayLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 20+(row*80), 30, 10)];
    dayLabel.text=[NSString stringWithFormat:@"%02d",day];
    dayLabel.textColor=[UIColor colorWithRed:60.0/255.0 green:60.0/255.0 blue:60.0/255.0 alpha:1];
    dayLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:14];
    [barChartWeekView addSubview:dayLabel];
    UIView *myBox  = [[UIView alloc] initWithFrame:CGRectMake(55, 20+(row*80), barlength*deskper, 10)];
    myBox.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:176.0/255.0 blue:207.0/255.0 alpha:1];
    [barChartWeekView addSubview:myBox];
    dayLabel=[[UILabel alloc]initWithFrame:CGRectMake(60+barlength*deskper, 20+(row*80), 50, 10)];
    dayLabel.text=[NSString stringWithFormat:@"%02d:%02d",stat.desk.hours,stat.desk.minutes];
    dayLabel.textColor=[UIColor colorWithRed:69.0/255.0 green:176.0/255.0 blue:207.0/255.0 alpha:1];
    dayLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    [barChartWeekView addSubview:dayLabel];
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(55, 40+(row*80), barlength*insideper, 10)];
    myBox.backgroundColor =[UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:63.0/255.0 alpha:1];
    [barChartWeekView addSubview:myBox];
    dayLabel=[[UILabel alloc]initWithFrame:CGRectMake(60+barlength*insideper, 40+(row*80), 50, 10)];
    dayLabel.text=[NSString stringWithFormat:@"%02d:%02d",stat.inside.hours,stat.inside.minutes];
    dayLabel.textColor=[UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:63.0/255.0 alpha:1];
    dayLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    [barChartWeekView addSubview:dayLabel];
    myBox  = [[UIView alloc] initWithFrame:CGRectMake(55, 60+(row*80), barlength*outsideper, 10)];
    myBox.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:182.0/255.0 blue:13.0/255.0 alpha:1];[UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:63.0/255.0 alpha:1];
    [barChartWeekView addSubview:myBox];
    dayLabel=[[UILabel alloc]initWithFrame:CGRectMake(60+barlength*outsideper, 60+(row*80), 50, 10)];
    dayLabel.text=[NSString stringWithFormat:@"%02d:%02d",stat.outside.hours,stat.outside.minutes];
    dayLabel.textColor=[UIColor colorWithRed:241.0/255.0 green:182.0/255.0 blue:13.0/255.0 alpha:1];
    dayLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14];
    [barChartWeekView addSubview:dayLabel];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
