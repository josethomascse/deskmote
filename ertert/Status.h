//
//  Status.h
//  StatusApp
//
//  Created by user on 6/24/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
enum Stat{inside_office=0,outside_office=1,atWork=2};

@interface Status : NSObject

-(NSDate *)getCurrentStartDate;
-(BOOL)retrieveDoor1EnterStatus;
-(BOOL)retrieveDoor1ExitStatus;
-(BOOL)retrieveDoor2EnterStatus;
-(BOOL)retrieveDoor2ExitStatus;
-(enum Stat)retrieveCurrentStatus;
-(enum Stat)retrievePvsStatus;
-(void)setDoor1EnterStatus:(BOOL)value;
-(void)setDoor2EnterStatus:(BOOL)value;
-(void)setDoor2ExitStatus:(BOOL)value;
-(void)setDoor1ExitStatus:(BOOL)value;
-(void)setCurrentStatus:(enum Stat)newState;
-(Status *)initialize;
-(void)saveToFile:(Status *)object;
-(int)getCurrentStartDuration;
-(NSDate *)retrieve_lastDeskexit;
-(void)set_lastDeskexit:(NSDate *)date;
-(UIColor *)retrieve_tintcolor;
@end
