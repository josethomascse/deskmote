//
//  Status.m
//  StatusApp
//
//  Created by user on 6/24/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "Status.h"
#import "StatisticsOperations.h"
#import "SettingsControl.h"

@implementation Status{
    
    enum Stat current_state;
    enum Stat pvs_state;
    NSDate *officeTimeIn;
    NSDate *officeTimeOut;
    NSDate *outOfficeTimeIn;
    NSDate *outOfficeTimeOut;
    NSDate *deskTimeIn;
    NSDate *deskTimeOut;
    NSDate *lastDeskexit;
    BOOL door1Enter;
    BOOL door1Exit;
    BOOL door2Enter;
    BOOL door2Exit;

}
-(NSDate *)retrieve_lastDeskexit{

    return lastDeskexit;
}
-(void)set_lastDeskexit:(NSDate *)date{
    lastDeskexit=date;
}
-(BOOL)retrieveDoor1EnterStatus{
    return door1Enter;
}

-(BOOL)retrieveDoor1ExitStatus{
    return door1Exit;
}

-(BOOL)retrieveDoor2EnterStatus{
    return door2Enter;
}

-(BOOL)retrieveDoor2ExitStatus{
    return door2Exit;
}

-(enum Stat)retrieveCurrentStatus{
    return current_state;
}

-(enum Stat)retrievePvsStatus{
    return pvs_state;
}
-(UIColor *)retrieve_tintcolor
{
    switch (current_state)
    {
        case 0:
            return [UIColor colorWithRed:129/255.0 green:199/255.0 blue:132/255.0 alpha:1.0];
            break;
        case 1:
            return [UIColor colorWithRed:255/255.0 green:213/255.0 blue:79/255.0 alpha:1.0];
            break;
        case 2:
            return [UIColor colorWithRed:77/255.0 green:208/255.0 blue:225/255.0 alpha:1.0];
            break;
        default:
            break;
    }
}

-(void)setCurrentStatus:(enum Stat)newState{
    SettingsControl *sControl=[[SettingsControl alloc]init];
    sControl=[sControl initialize];
    StatisticsOperations *stat=[[StatisticsOperations alloc]init];
    NSDate *dbDate=[[NSDate alloc]init];
    NSDate *todayDate=[NSDate date];
    
    NSTimeInterval secondsBetween,fullDaySeconds,elapsedLastDaySeconds,elapsedTodaySeconds,secondsBetween2;
    if (current_state==0) {
        officeTimeOut=todayDate;
        secondsBetween = [officeTimeOut timeIntervalSinceDate:officeTimeIn];
        elapsedLastDaySeconds = 86400- [sControl getTime:officeTimeIn];
        dbDate=officeTimeIn;
    }else if(current_state==1){
        outOfficeTimeOut=todayDate;
        secondsBetween = [outOfficeTimeOut timeIntervalSinceDate:outOfficeTimeIn];
        elapsedLastDaySeconds = 86400- [sControl getTime:outOfficeTimeIn];
        dbDate=outOfficeTimeIn;
    }else{
        deskTimeOut=todayDate;
        secondsBetween = [deskTimeOut timeIntervalSinceDate:deskTimeIn];
        elapsedLastDaySeconds = 86400- [sControl getTime:deskTimeIn];
         dbDate=deskTimeIn;
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MM yyyy"];
    NSString *todayString=[dateFormat stringFromDate:todayDate];
    NSString *dbString=[dateFormat stringFromDate:dbDate];
    if([todayString  isEqualToString:dbString]){
        secondsBetween=[sControl getWorkIntervalTime:dbDate interval:secondsBetween];
        if (current_state==0) {
            [stat appendInsideOfficeTime:secondsBetween date:dbDate];
        }else if(current_state==1){
            [stat appendOutsideOfficeTime:secondsBetween date:dbDate];
        }else{
            [stat appendAtWorkTime:secondsBetween date:dbDate];
        }

    }else{
        elapsedTodaySeconds = [sControl getTime:todayDate];
        fullDaySeconds = (secondsBetween - elapsedTodaySeconds)-elapsedLastDaySeconds;
        float days;
        days=fullDaySeconds/86400;
        days=lroundf(days);
        NSDateComponents *components= [[NSDateComponents alloc] init];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *date=[[NSDate alloc]init];
        date=todayDate;
        while(days>0){
            [components setDay:-1];
            date= [calendar dateByAddingComponents:components toDate:date options:0];
            secondsBetween=[sControl getWorkTime];
            if (current_state==0) {
                [stat appendInsideOfficeTime:secondsBetween date:date];
            }else if(current_state==1){
                [stat appendOutsideOfficeTime:secondsBetween date:date];
            }else{
                [stat appendAtWorkTime:secondsBetween date:date];
            }
            days--;
        }
        secondsBetween=[sControl getWorkIntervalTime:dbDate interval:elapsedLastDaySeconds];
        date=[dateFormat dateFromString:todayString];
        secondsBetween2=[sControl getWorkIntervalTime:date interval:elapsedTodaySeconds];
        if (current_state==0) {
            [stat appendInsideOfficeTime:secondsBetween date:dbDate];
            [stat appendInsideOfficeTime:secondsBetween2 date:date];
        }else if(current_state==1){
            [stat appendOutsideOfficeTime:secondsBetween date:dbDate];
            [stat appendOutsideOfficeTime:secondsBetween2 date:date];
        }else{
            [stat appendAtWorkTime:secondsBetween date:dbDate];
            [stat appendAtWorkTime:secondsBetween2 date:date];
        }

    }
    
    if (newState==0) {
        officeTimeIn=todayDate;
    }else if(newState==1){
        outOfficeTimeIn=todayDate;
    }
    else{
        deskTimeIn=todayDate;
    }
    if(newState != current_state){
        pvs_state=current_state;
        current_state=newState;
    }
}

-(NSDate *)getCurrentStartDate{
    if (current_state==0) {
       return officeTimeIn;
    }
    else if(current_state==1){
        
         return outOfficeTimeIn;
    }
    else{
        return deskTimeIn;
    }

}

-(int)getCurrentStartDuration{
    NSTimeInterval secondsBetween;
    if (current_state==0) {
          secondsBetween= [[NSDate date] timeIntervalSinceDate:officeTimeIn];
            }
    else if(current_state==1){
         secondsBetween = [[NSDate date] timeIntervalSinceDate:outOfficeTimeIn];
       
    }
    else{
         secondsBetween = [[NSDate date] timeIntervalSinceDate:deskTimeIn];
    }
    return secondsBetween;
}

-(void)setDoor1EnterStatus:(BOOL)value{
    door1Enter=value;
}

-(void)setDoor2EnterStatus:(BOOL)value{
    door2Enter=value;
}

-(void)setDoor2ExitStatus:(BOOL)value{
    door2Exit=value;
}

-(void)setDoor1ExitStatus:(BOOL)value{
    door1Exit=value;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
  
   self->current_state  =[decoder decodeIntForKey:@"1"];
   self->officeTimeIn  = [decoder decodeObjectForKey:@"2"];
   self->officeTimeOut  = [decoder decodeObjectForKey:@"3"];
   self->outOfficeTimeIn  = [decoder decodeObjectForKey:@"4"];
   self->outOfficeTimeOut  = [decoder decodeObjectForKey:@"5"];
   self->deskTimeIn  = [decoder decodeObjectForKey:@"6"];
   self->deskTimeOut = [decoder decodeObjectForKey:@"7"];
   self->door1Enter  =[decoder decodeIntForKey:@"8"];
   self->door1Exit  =[decoder decodeIntForKey:@"9"];
   self->door2Enter  =[decoder decodeIntForKey:@"10"];
   self->door2Exit  =[decoder decodeIntForKey:@"11"];
   self->pvs_state  =[decoder decodeIntForKey:@"12"];
   self->lastDeskexit  =[decoder decodeObjectForKey:@"13"];
    
   return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:self->current_state forKey:@"1"];
    [encoder encodeObject:self->officeTimeIn forKey:@"2"];
    [encoder encodeObject:self->officeTimeOut forKey:@"3"];
    [encoder encodeObject:self->outOfficeTimeIn forKey:@"4"];
    [encoder encodeObject:self->outOfficeTimeOut forKey:@"5"];
    [encoder encodeObject:self->deskTimeIn forKey:@"6"];
    [encoder encodeObject:self->deskTimeOut forKey:@"7"];
    [encoder encodeInt:self->door1Enter forKey:@"8"];
    [encoder encodeInt:self->door1Exit forKey:@"9"];
    [encoder encodeInt:self->door2Enter forKey:@"10"];
    [encoder encodeInt:self->door2Exit forKey:@"11"];
    [encoder encodeInt:self->pvs_state forKey:@"12"];
    [encoder encodeObject:self->lastDeskexit forKey:@"13"];
  }



-(Status *)initialize{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathUrl = [NSString stringWithFormat:@"statusconfiguration.txt"];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pathUrl];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        Status *tempObject = [[Status alloc]init];
        tempObject = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        return tempObject;
    }
    officeTimeOut=[NSDate date];
    outOfficeTimeOut=[NSDate date];
    outOfficeTimeIn=[NSDate date];
    deskTimeOut=[NSDate date];
    current_state=outside_office;
    pvs_state=inside_office;
    lastDeskexit=nil;
    door2Exit=false;
    [self saveToFile:self];
    return self;
}

-(void)saveToFile:(Status *)object{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathUrl = [NSString stringWithFormat:@"statusconfiguration.txt"];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:pathUrl];
    [NSKeyedArchiver archiveRootObject:object toFile:path];
    

}

@end
