//
//  ToDoListTableViewController.h
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ToDoListTableViewControllerDelegate <NSObject>

@end
@interface ToDoListTableViewController : UITableViewController<ToDoListTableViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *upcominglabel;

@end
