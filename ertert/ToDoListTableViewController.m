//
//  ToDoListTableViewController.m
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "ToDoListTableViewController.h"
#import "reminderCell.h"
#import "SettingsControl.h"
#import "DatePickerViewController.h"
#import "Status.h"
#import "AppDelegate.h"
@interface ToDoListTableViewController ()
{
    NSMutableArray *reminder_Array;
    UILocalNotification *editednote;
    
}
@end

@implementation ToDoListTableViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
           }
    return self;
}


- (void)viewDidLoad {

   

    
    [super viewDidLoad];
   
    
         [self reloadTable ];
   

       self.tableView.estimatedRowHeight = 30.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;

   }
-(void)viewWillAppear:(BOOL)animated
{

    [self reloadTable];
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable)
                                                 name:@"reloadData"
                                               object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(changeNavBarColor)
                                                  name:@"sate_change"
                                                object:nil];
    [self reloadTable ];
    
}
-(void)changeNavBarColor{
    Status *st=[[Status alloc]initialize];
    self.navigationController.navigationBar.barTintColor=[st retrieve_tintcolor];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                name:@"reloadData"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sate_change" object:nil];

}
-(void)animationDidStart:(CAAnimation *)anim
{
        [self reloadTable ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { 

    // Return the number of sections.
   return 1;
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
       return [reminder_Array count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    reminderCell *cell;

    {
        
        static NSString *CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        UILocalNotification *localNotification = [reminder_Array objectAtIndex:indexPath.row];
        [cell.alert setText:localNotification.alertBody];
        
        
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@" dd-MM-yyyy hh:mm a "];
       NSString *dateString= [dateformatter stringFromDate:localNotification.fireDate ];
        [cell.time setText:dateString];
        cell.view.layer.cornerRadius = 6;
        cell.view.layer.masksToBounds = NO;
        cell.view.layer.borderWidth = 0;
        [cell.view.layer setShadowColor:[UIColor lightGrayColor].CGColor];
        [cell.view.layer setShadowOpacity:1];
        [cell.view.layer setShadowRadius:1.2];
        [cell.view.layer setShadowOffset:CGSizeMake(-2.0, 2.0)];

        return cell;
    }
}




- (void)reloadTable
{
    [self load_reminderArray];
    [self.tableView reloadData];
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        UILocalNotification *localNotification = [reminder_Array objectAtIndex:indexPath.row];
        
        [reminder_Array removeObject:localNotification];
        [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    //     [self setBadgenumber];
         [self reloadTable ];
    
}


-(void)load_reminderArray
{
    reminder_Array=[[NSMutableArray alloc]init];
    NSDictionary *userInfoCurrent;
    NSString *uid;
    UILocalNotification *localNotification;
    
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSInteger count=[localNotifications count];
    
  
    
    for (int i=0; i<count; i++) {
        localNotification = [localNotifications objectAtIndex:i];
        userInfoCurrent = localNotification.userInfo;
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"reminder"]];
        if(![uid isEqualToString:@"(null)"])
        {
            [reminder_Array addObject:localNotification];
            
            
        }
    }
    if([reminder_Array count]==0)
    {
        [_upcominglabel setText:@""];
    }
    else
    {
        [_upcominglabel setText:@"Upcoming"];
    }

}
/*
-(void)setBadgenumber
{
    int badgeNumber=1,flag;
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    NSInteger count=[localNotifications count];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
    
    
    for (int i=0; i<count; i++) {
        flag=1;
        localNotification = [localNotifications objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
        if(![uid isEqualToString:@"(null)"])
        {
            flag=0;
        }
        if (1==flag) {
            localNotification.applicationIconBadgeNumber=badgeNumber;
            badgeNumber=badgeNumber+1;
        }
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}*/

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    UILocalNotification *note=[reminder_Array objectAtIndex:indexPath.row];
    
    DatePickerViewController *popController1 = [[DatePickerViewController alloc] init];
    popController1.data=note.alertBody;
    popController1.defaultdate=note.fireDate;
    popController1.notify=note;
    popController1.flag=0;
    popController1.contentSize = CGSizeMake(300, 45);
    popController1.arrowDirection = 0;
    
    [self presentViewController:popController1
                       animated:YES
                     completion:nil];
   }


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
