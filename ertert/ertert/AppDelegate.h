//
//  AppDelegate.h
//  ertert
//
//  Created by user on 6/8/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <EstimoteSDK/EstimoteSDK.h>
#import "MenuTableViewController.h"
#import "NavigationBar.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property (retain,nonatomic) IBOutlet UIScreenEdgePanGestureRecognizer *pan;
@property (retain,nonatomic) IBOutlet UITapGestureRecognizer *tap;
@property (retain,nonatomic) IBOutlet UISwipeGestureRecognizer *swipe;
@property (retain,nonatomic) UIView *slidingMenu;
@property (retain,nonatomic) UIView *veilView;
@property (retain,nonatomic) NavigationBar *navBar;
@property (retain,nonatomic) MenuTableViewController *table;

- (IBAction)slidingmenuback:(id)sender ;
@end

