//
//  AppDelegate.m
//  ertert
//
//  Created by user on 6/8/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "status.h"
#import "SettingsControl.h"
#import <UIKit/UIKit.h>
#import "StatisticsOperations.h"
//#include ""



@interface AppDelegate ()<ESTBeaconManagerDelegate,UISplitViewControllerDelegate>
@property (nonatomic, strong) ESTBeaconManager *beaconManager1;
//@property (nonatomic, strong) CLBeaconRegion *region_door1;
//@property (nonatomic, strong) CLBeaconRegion *region_door2;
//@property (nonatomic, strong) CLBeaconRegion *region_desk;


@end

@implementation AppDelegate

CGRect screenRect5;
CGFloat screenWidth5;
CGFloat screenHeight5;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification!=nil)
    {
        [self application:application didReceiveLocalNotification:notification];
    }
    


     [ESTCloudManager setupAppID:@"app_0yva4sqvw6" andAppToken:@"fa64385bf2d0cfb6d35d5591547b68bb"];
//    [ESTCloudManager enableMonitoringAnalytics:YES];
//    [ESTCloudManager enableGPSPositioningForAnalytics:YES];

    
    
    SettingsControl *settingsFile;
    settingsFile=[[SettingsControl alloc]init];
    settingsFile=[settingsFile initialize];
    

   //------------------Beacon Manager Part Start Monitoring----------------------------------------
        self.beaconManager1=[[ESTBeaconManager alloc]init];
        self.beaconManager1.delegate=self;
    
    
            NSSet *set=[self.beaconManager1 monitoredRegions];
        
             //already monitoring
             if ([set count]<3) {
        
               // Initialising regions
                self.beaconManager1.avoidUnknownStateBeacons=YES;
                self.beaconManager1.preventUnknownUpdateCount=YES;
                 
                 
                 for (int i=0; i<17; i++) {
                     [self.beaconManager1 startMonitoringForRegion: [[CLBeaconRegion alloc]
                                                                     initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID major:i minor:i+1
                                                                     identifier:@"list"]];
                 }

                 
                
                 
                CLBeaconRegion *region_desk=[[CLBeaconRegion alloc]
                                  initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID major:36798 minor:29499
                                    identifier:@"Desk_Beacon_Region"];
        
                CLBeaconRegion *region_door1=[[CLBeaconRegion alloc]
                                   initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID major:64157 minor:33188
                                   identifier:@"Door_Beacon1_Region"];
        
                CLBeaconRegion *region_door2=[[CLBeaconRegion alloc]
                                   initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID major:29666 minor:63757
                                   identifier:@"Door_Beacon2_Region"];

                 
                [self.beaconManager1 requestAlwaysAuthorization];
                if (![set containsObject:region_desk]) {
                    [self.beaconManager1 startMonitoringForRegion: region_desk];
                }
                if (![set containsObject:region_door1]) {
                    [self.beaconManager1 startMonitoringForRegion: region_door1];
                }
                if (![set containsObject:region_door2]) {
                    [self.beaconManager1 startMonitoringForRegion: region_door2];
                }
            }
    
//---------------------------------------------------------------------------------------------------
   
    
    
    //-------------------------- Persmission to show Local Notification.-------------------------------------
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    //--------------------------------------------------------------------------------------------------------
    

    
    //------------------This Code execute exactly once after the app is installed---------------------------
        if (![settingsFile installed])
    {
      
        //cancell  all notifications
        [[UIApplication sharedApplication]cancelAllLocalNotifications];

        
        
        // For App Start silent notification
        if ([[NSDate date] compare:[settingsFile retrieveOfficeStart]]==NSOrderedDescending)
            [self send_appstart:[NSDate dateWithTimeInterval:3600*24 sinceDate:[settingsFile retrieveOfficeStart]]];
        else
               [self send_appstart:[settingsFile retrieveOfficeStart]];
        
        
        // For App stop silent notification
        if ([[NSDate date] compare:[settingsFile retrieveOfficeEnd]]==NSOrderedDescending)
            [self send_appstop:[NSDate dateWithTimeInterval:3600*24 sinceDate:[settingsFile retrieveOfficeEnd]]];
        else
              [self send_appstop:[settingsFile retrieveOfficeEnd]];
        
        //for lunch start silent notification
        if ([[NSDate date] compare:[settingsFile retrieveLunchStart]]==NSOrderedDescending)
            [self lunch_startsilent:[NSDate dateWithTimeInterval:3600*24 sinceDate:[settingsFile retrieveLunchStart]]];
        else
            [self lunch_startsilent:[settingsFile retrieveLunchStart]];
        
        //for lunch stop silent notification
        if ([[NSDate date] compare:[settingsFile retrieveLunchEnd]]==NSOrderedDescending)
            [self lunch_stopsilent:[NSDate dateWithTimeInterval:3600*24 sinceDate:[settingsFile retrieveLunchEnd]]];
        else
            [self lunch_stopsilent:[settingsFile retrieveLunchEnd]];
 
        
        //for lunch stop silent notification
        [self send_12pmalertsilent:[self Twelvepm_date]];
            
      
    
        //set the app is installed
        [settingsFile setinstalled:true];
        [settingsFile saveToFile:settingsFile];
        
      
    }
    //--------------------------------------------------------------------------------------------------
    
    
    
    
 

    

    

    
    
    
   

    //------------ Override point for customization after application launch.--------------------------
    MenuTableViewController *masterViewController = [[MenuTableViewController alloc] init];
    UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    
   ViewController *detailViewController = [[ViewController alloc] init];
    UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    
    UISplitViewController* splitViewController = [[UISplitViewController alloc] init];
    splitViewController.delegate = self;
    splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];
    //--------------------------------------------------------------------------------------------------
    //[NSThread sleepForTimeInterval:1.0];
          //  [self  send_healthwarning:@"OK"];
   
    //jose
    Status *st = [[Status alloc]init];
    st=[st initialize];
    [st setCurrentStatus:[st retrieveCurrentStatus]];
    [st saveToFile:st];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change"  object:self];
    
    //-------------------------------------------
    self.pan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(panaction:)];
    self.tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(slidingmenuback:)];
    self.swipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slidingmenuback:)];
    
    [self.swipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.pan setEdges:UIRectEdgeLeft];
    [self.window addGestureRecognizer:self.pan];
    
    screenRect5 = [[UIScreen mainScreen] bounds];
    screenWidth5 = screenRect5.size.width;
    screenHeight5 = screenRect5.size.height;
    
    self.slidingMenu=[[UIView alloc]initWithFrame:CGRectMake(-0.8*screenWidth5, 0, 0.8*screenWidth5, screenHeight5)];
    self.slidingMenu.layer.shadowColor=[[UIColor blackColor]CGColor];
    self.slidingMenu.layer.shadowOpacity=1;
    self.slidingMenu.layer.shadowRadius=100;
    self.slidingMenu.layer.shadowOffset=CGSizeMake(100, 0);
    
    self.veilView=[[UIView alloc]initWithFrame:CGRectMake(0.8*screenWidth5, 0, 0.2*screenWidth5, screenHeight5)];
    self.veilView.backgroundColor=[UIColor clearColor];
    [self.veilView addGestureRecognizer:self.tap];
    [self.veilView addGestureRecognizer:self.swipe];
    self.veilView.hidden = true;
    
    self.navBar=[[NavigationBar alloc]initWithFrame:CGRectMake(0, 0,screenWidth5, 63)];
    [self.navBar initialize];
    self.navBar.backgroundColor=[UIColor whiteColor];
    self.navBar.layer.shadowColor=[[UIColor blackColor]CGColor];
    self.navBar.layer.shadowOpacity=1;
    self.navBar.layer.shadowRadius=3;
    self.navBar.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.navBar.layer.borderWidth=0.5;
    
    if(![settingsFile retrieveDbStatus])
        self.navBar.backgroundColor=[st retrieve_tintcolor];
    
    UIButton *slide=[[UIButton alloc]init];
    slide=[UIButton buttonWithType:UIButtonTypeCustom];
    slide.frame=CGRectMake(0, 0, 50, 60);
    UIImageView *buttonImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 35, 16, 16)];
    buttonImage.image= [UIImage imageNamed:@"ic_menu.png"];
    [slide addSubview:buttonImage];
//    [slide setBackgroundImage:[UIImage imageNamed:@"ic_menu.png"]
//                     forState:UIControlStateNormal];
    [slide addTarget:self action:@selector(slidingmenucome:) forControlEvents:UIControlEventTouchUpInside];
    [self.navBar addSubview:slide];
    
    
    
    
    //self.slidingMenu.
    //    MenuTableViewController *tableController=[[MenuTableViewController alloc]init];
    //    tableController.tableView=[[UITableView alloc]init];
    //    [tableController viewDidLoad];
    //    [tableController viewDidAppear:1];
    //   // [tableController.tableView reloadData];
    //    [self.slidingMenu addSubview:tableController.view];
    self.slidingMenu.clipsToBounds=YES;
    //    ViewControllerB *viewCotrollerB = [storyboard instantiateViewControllerWithIdentifier:@"ViewControllerB"];
    //    [self presentModalViewController:viewControllerB animated:YES];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.table = [storyboard instantiateViewControllerWithIdentifier:@"Table"];
    self.table.view.frame = self.slidingMenu.bounds;
    self.table.selected=0;
    [self.slidingMenu addSubview:self.table.view];
    //    [self.slidingMenu addChildViewController:table];
    //   [table didMoveToParentViewController:self];
//-------------sliding menu---------------
    
    return YES;
    }



- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void(^)(NSDictionary *replyInfo))reply
{
    Status*s=[[Status alloc]initialize];
    NSString *returnstring;
    NSString *uid=[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"reply"]];
    if ([uid isEqualToString:@"reply1"])
    {
    

        switch ([s retrieveCurrentStatus]) {
            case 0:
                returnstring=@"office.png";
                break;
            case 1:
                returnstring=@"outdoor.png";
                break;
            case 2:
                returnstring=@"work.png";
                break;
                
            default:
                break;
        }
    
    }
    else
    {
        
        int totaltime;
        StatisticsOperations *st=[[StatisticsOperations alloc]init];
        struct statObject stat=[st retrieveTimeObject:[NSDate date] seconds:1 realTime:0];
        switch ([s retrieveCurrentStatus])
        {
            case 0:
                totaltime=stat.inside.seconds+[s getCurrentStartDuration];
                break;
            case 1:
                totaltime=stat.outside.seconds+[s getCurrentStartDuration];
                break;
            case 2:
                totaltime=stat.desk.seconds+[s getCurrentStartDuration];
                break;
                
            default:
                break;
        }
        int minit=(totaltime-fmod(totaltime, 60))/60;
        int hour=(minit-fmod(minit, 60))/60l;
        minit=fmod(minit, 60);
        returnstring=[NSString stringWithFormat:@"%02d:%02d",hour,minit];
        
        
        SettingsControl *set=[[SettingsControl alloc]initialize];
        if ([set retrieveDbStatus]) {
            returnstring=@"App is inactive";
        }
        
    }

  NSDictionary *info=[NSDictionary dictionaryWithObject:returnstring forKey:@"reply"];
    reply(info);
   
}


//send 12pm alert
-(void)send_12pmalertsilent:(NSDate *)firedate
{
    UILocalNotification *Twelvepm= [[UILocalNotification alloc] init];
    Twelvepm.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Start tha app" forKey:@"12pm"];
    Twelvepm.userInfo=info;
    Twelvepm.timeZone = [NSTimeZone defaultTimeZone];
    Twelvepm.repeatInterval=NSCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:Twelvepm];
}
//12pmdate
-(NSDate *)Twelvepm_date
{
    //date string
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MM yyyy"];
    NSString *datestring=[dateFormat stringFromDate:[NSDate date]];
    
    //time string
    [dateFormat setDateFormat:@"hh:mm:ss a"];
    NSString *timestring=@"12:00:00 AM";
    
    [dateFormat setDateFormat:@"dd MM yyyy hh:mm:ss a"];
    
    // [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    return [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",datestring,timestring]];
}

//send appstart
-(void)send_appstart:(NSDate *)firedate
{
    UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
    officeTimings.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Start tha app" forKey:@"startapp"];
    officeTimings.userInfo=info;
    officeTimings.timeZone = [NSTimeZone defaultTimeZone];

    [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
        officeTimings.repeatInterval=NSCalendarUnitDay;
    
}

//send appstop
-(void)send_appstop:(NSDate *)firedate
{
    
    UILocalNotification *officeTimings= [[UILocalNotification alloc] init];
    officeTimings.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"Stop the app" forKey:@"stopapp"];
    officeTimings.userInfo=info;
    officeTimings.timeZone = [NSTimeZone defaultTimeZone];

    [[UIApplication sharedApplication] scheduleLocalNotification:officeTimings];
        officeTimings.repeatInterval=NSCalendarUnitDay;
}


//send lunch start silent notification
-(void)lunch_startsilent:(NSDate *)firedate
{
    UILocalNotification *lunchstart= [[UILocalNotification alloc] init];
    lunchstart.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"not nil" forKey:@"lunchstartsilent"];
    lunchstart.userInfo=info;
    lunchstart.timeZone = [NSTimeZone defaultTimeZone];

    [[UIApplication sharedApplication] scheduleLocalNotification:lunchstart];
        lunchstart.repeatInterval=NSCalendarUnitDay;
}


//send lunch stop silent notification
-(void)lunch_stopsilent:(NSDate *)firedate
{
    UILocalNotification *lunchstop= [[UILocalNotification alloc] init];
    lunchstop.fireDate=firedate;
    NSDictionary *info=[NSDictionary dictionaryWithObject:@"not nil" forKey:@"lunchstopsilent"];
    lunchstop.userInfo=info;
    lunchstop.timeZone = [NSTimeZone defaultTimeZone];

    [[UIApplication sharedApplication] scheduleLocalNotification:lunchstop];
        lunchstop.repeatInterval=NSCalendarUnitDay;
}


//send health_warning
-(void)send_healthwarning:(NSString *)text
{
    SettingsControl *s=[[SettingsControl alloc]init];
    s=[s initialize];
    
    if ([s retrieveHealthWarningsStatus])
    {
        UILocalNotification *healthwarning = [[UILocalNotification alloc] init];
        healthwarning.fireDate=[NSDate dateWithTimeIntervalSinceNow:7200];
        healthwarning.repeatInterval=NSCalendarUnitHour;
        healthwarning.alertBody = [NSString stringWithFormat:@"%@",text];
        healthwarning.alertAction=@"Show me the item";
        healthwarning.soundName = UILocalNotificationDefaultSoundName;
        healthwarning.timeZone = [NSTimeZone defaultTimeZone];
        NSDictionary *info=[NSDictionary dictionaryWithObject:text forKey:@"health_warning"];
        healthwarning.userInfo=info;
        [[UIApplication sharedApplication] scheduleLocalNotification:healthwarning];
    }
//        [self setBadgenumber];

    
}


-(void)cancel_allNotifications
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification *localNotification = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = localNotification.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"health_warning"]];
        if(![uid isEqualToString:@"(null)"])
        {
            [app cancelLocalNotification:localNotification];
        }
    }
//    [self setBadgenumber];
}



////---------Set badge number-----------------------
//-(void)setBadgenumber
//{
//    int badgeNumber=1,flag;
//    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
//    NSInteger count=[localNotifications count];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    UILocalNotification *localNotification;//=[[UILocalNotification alloc]init];
//    
//    
//    for (int i=0; i<count; i++) {
//        flag=1;
//        localNotification = [localNotifications objectAtIndex:i];
//        NSDictionary *userInfoCurrent = localNotification.userInfo;
//        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//        uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
//        if(![uid isEqualToString:@"(null)"])
//        {
//            flag=0;
//        }
//       
//            localNotification.applicationIconBadgeNumber=badgeNumber;
//        if (1==flag)
//        {
//            badgeNumber=badgeNumber+1;
//        }
//        
//        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    }
//    
//}



- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{

    
    
  UIApplicationState state = [application applicationState];


    //-------------------file---------------------
    SettingsControl *settingsFile;
    settingsFile=[[SettingsControl alloc]init];
    settingsFile=[settingsFile initialize];
    Status *st=[[Status alloc]init];
    st=[st initialize];
    
    

    NSDictionary *userInfoCurrent = notification.userInfo;
    
    //-----------------health warning dealing-----------------------
    NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"health_warning"]];
    if(![uid isEqualToString:@"(null)"])
    {
        
    }
    
    //---------------Run app only in office timings-------------------
    
    //OFFICE START ALERT
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"startapp"]];
    if(![uid isEqualToString:@"(null)"])
    {
        [st setCurrentStatus:[st retrieveCurrentStatus]];
        [st saveToFile:st];
        self.navBar.backgroundColor=[st retrieve_tintcolor];
        //make the effect in home screen if app is in foreground
        if (state == UIApplicationStateActive)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change"  object:self];
        }
        return;

    }
    
    // OFFICE STOP ALERT
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"stopapp"]];
    if(![uid isEqualToString:@"(null)"])
    {
                [st setCurrentStatus:[st retrieveCurrentStatus]];
        [st saveToFile:st];
          self.navBar.backgroundColor=[UIColor whiteColor];
        //make the effect in home screen if app is in foreground
        if (state == UIApplicationStateActive)
        {
            [ [NSNotificationCenter defaultCenter] postNotificationName:@"sate_change" object:self];
        }
        return;
    }
    
    
    //LUNCH START ALERT
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstartsilent"]];
    if(![uid isEqualToString:@"(null)"])
    {
          [st setCurrentStatus:[st retrieveCurrentStatus]];
        [st saveToFile:st];
          self.navBar.backgroundColor=[UIColor whiteColor];
        //make the effect in home screen if app is in foreground
        if (state == UIApplicationStateActive)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change"  object:self];
        }
        return;
        
    }
    
    // LUNCH STOP ALERT
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"lunchstopsilent"]];
    if(![uid isEqualToString:@"(null)"])
    {
               [st setCurrentStatus:[st retrieveCurrentStatus]];
        [st saveToFile:st];
          self.navBar.backgroundColor=[st retrieve_tintcolor];
        //make the effect in home screen if app is in foreground
        if (state == UIApplicationStateActive)
        {
            [ [NSNotificationCenter defaultCenter] postNotificationName:@"sate_change" object:self];
        }
        return;
    }
    //-----------------------------------------------------------------
    // 12pm alert
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"12pm"]];
    if(![uid isEqualToString:@"(null)"])
    {
        [st setCurrentStatus:[st retrieveCurrentStatus]];
        [st saveToFile:st];
        //add code here:
        if (state == UIApplicationStateActive)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change"  object:self];
        }
        return;
    }
    //-----------------------------------------------------------------
    uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"reminder"]];
    if(![uid isEqualToString:@"(null)"])
    {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
        if (state!=UIApplicationStateActive)
        {
            self.table.selected=2;
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *aYourViewController = [storyboard   instantiateViewControllerWithIdentifier:@"Reminder"];
            [self.window setRootViewController:aYourViewController ];// presentModalViewController:aYourViewController animated:NO completion:nil];

        }
        
        
    }
    //-----------------------------------------------------------------
    
    //alert showing if app is active

    if (state==UIApplicationStateActive)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DESKMOTE"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"Close"
                                              otherButtonTitles:nil];
        
        [alert show];
    
           // [UIApplication sharedApplication].applicationIconBadgeNumber =   0;
          //  [self setBadgenumber];

    }

    
    //save to file
    [st saveToFile:st];
}




















#pragma mark - ESTBeaconManager delegate

-(void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
    
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody = [NSString stringWithFormat:@"Error in monitoring %@ %@",region.identifier,error.description];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    
    
}
-(void) beaconManager:(id)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined)
    {
        [self.beaconManager1 requestAlwaysAuthorization];

//        UILocalNotification *notification = [[UILocalNotification alloc] init];
//        notification.alertBody = [NSString stringWithFormat:@"Status changed kCLAuthorizationStatusNotDetermined "];
//        notification.soundName = UILocalNotificationDefaultSoundName;
//       [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        
    }
    else if(status == kCLAuthorizationStatusAuthorizedAlways)
    {
//        
//        UILocalNotification *notification = [[UILocalNotification alloc] init];
//        notification.alertBody = [NSString stringWithFormat:@"Status changed  kCLAuthorizationStatusAuthorizedAlways"];
//        notification.soundName = UILocalNotificationDefaultSoundName;
//        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        
    }
    if(status == kCLAuthorizationStatusDenied)
    {


        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Status changed  kCLAuthorizationStatusDenied"];
        notification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        [self.beaconManager1 requestAlwaysAuthorization];

    }
    else if(status== kCLAuthorizationStatusRestricted)
    {
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.alertBody = [NSString stringWithFormat:@"Status changed  kCLAuthorizationStatusDenied"];
        notification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        [self.beaconManager1 requestAlwaysAuthorization];

        
    }
    
    
    
}

//--------------Enter Region Function----------------------------

-(void)beaconManager:(ESTBeaconManager *)manager
      didEnterRegion:(CLBeaconRegion *)region
{
//    
//    [manager stopMonitoringForRegion:region];
//    [ manager startMonitoringForRegion:region];

//       [self.beaconManager1 requestAlwaysAuthorization];
//if ([ESTBeaconManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
//{
//      UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody = [NSString stringWithFormat:@"Entered because no authorisation%@",region.identifier];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//
//
//}
//else
//{
//
//    
//        UILocalNotification *notification = [[UILocalNotification alloc] init];
//        notification.alertBody = [NSString stringWithFormat:@"Entered%@",region.identifier];
//        notification.soundName = UILocalNotificationDefaultSoundName;
//        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//}

    int state_changed=0;

    
    //        inside_office=0,
    //        outside_office=1,
    //        atWork=2
    
    //Opening File
    Status *s=[[Status alloc]init];
    s=[s initialize];
    
    //Logic for State Change
    int state=[s retrieveCurrentStatus]; //fetch current state
    
    if ([region.identifier isEqualToString:@"Desk_Beacon_Region"]) {
        
        if (state!=2) {//status is not atwork
            [s setCurrentStatus:2];//@"atwork"
            [self cancel_allNotifications];
            state_changed=1;
            
            
            NSLog(@"Changed status to at work"); //SCHEDULING NOTIFICATION  TAKE A BREAK FROM WORK
            [self send_healthwarning:@"Go and get a coffe!"];
            
            [s setDoor1EnterStatus:FALSE];
            [s setDoor2EnterStatus:FALSE];
            [s setDoor1ExitStatus:FALSE];
            [s setDoor2ExitStatus:FALSE];
        }
        
    }
    else if([region.identifier isEqualToString:@"Door_Beacon2_Region" ])
    {
        if(state==1 && [s retrieveDoor1EnterStatus] )// if current_state==outsideoffice && door_1enter==true
        {
            [s setCurrentStatus:0];//@"inside office"
            state_changed=1;
            [self cancel_allNotifications];
  
            
            
            //SCHEDULING NOTIFICATION  GO BACK TO WORK WORK
            [self send_healthwarning:@"Working Time, Go back to desk"];

        }
        [s setDoor2EnterStatus:TRUE];
        [s setDoor2ExitStatus:FALSE];
        //door_2enter=true;
    }
    else
    {
        [s setDoor1EnterStatus:TRUE];//door_2enter=true;
    }


      
    [s saveToFile:s];//save file
    if (state_changed==1 ) {
       // UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        SettingsControl *sc=[[SettingsControl alloc]initialize];
        if(![sc retrieveDbStatus])
             self.navBar.backgroundColor=[s retrieve_tintcolor];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change" object:self];
      
               //
    }
    
    //         if region==desk
    //            state_change("atwork");
    //            door_1enter=false;
    //            door_2enter=false;
    //            door_1exit=false;
    //            door_1exit=false;
    //
    //         else if region==door_2
    //            if current_state==outsideoffice && door_1enter==true
    //              state_change("inside");
    //            door_2enter=true;
    //
    //         elseif region==door_1
    //            door_2enter=true;
    //
    
    
}
//
//- (NSUInteger) supportedInterfaceOrientations {
//    // Return a bitmask of supported orientations. If you need more,
//    // use bitwise or (see the commented return).
//    return UIInterfaceOrientationMaskPortrait;
//    // return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
//}
//
//- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
//    // Return the orientation you'd prefer - this is what it launches to. The
//    // user can still rotate. You don't have to implement this method, in which
//    // case it launches in the current orientation
//    return UIInterfaceOrientationPortrait;
//}
//---------------------------------------------------------------------------------------

//------------------This method is called when app enter to a region--------------------

-(void)beaconManager:(ESTBeaconManager *)manager
       didExitRegion:(CLBeaconRegion *)region
{

    

     //  [self.beaconManager1 requestAlwaysAuthorization];
//    [manager stopMonitoringForRegion:region];
//    [ manager startMonitoringForRegion:region];

    int state_changed=0;
    
//    if ([ESTBeaconManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
//    {
//        UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody =[NSString stringWithFormat:@"Exited since authorisation not determined   %@",region.identifier];
//    notification.soundName = UILocalNotificationDefaultSoundName;
//    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//    }
//    
//    else
//    {
    
//        UILocalNotification *notification = [[UILocalNotification alloc] init];
//        notification.alertBody =[NSString stringWithFormat:@"Exited   %@",region.identifier];
//        notification.soundName = UILocalNotificationDefaultSoundName;
//        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
//
//    }
    //Opening File
    Status *s=[[Status alloc]init];
    s=[s initialize];    //Logic for State Change
    int state=[s retrieveCurrentStatus]; //fetch current state
    //        inside_office=0,
    //        outside_office=1,
    //        atWork=2
    
    if ([region.identifier isEqualToString:@"Desk_Beacon_Region" ] ) {
        
//        //---if timer limit is not enough discard/avoid the perticular notification
//        if ([self discard_exitnotification]) {
        
//            return;
//        }
//        
//        //---------------------------------------
        if (state==2) {//status is atwork
            [s setCurrentStatus:0];//@"status changed to inside office"
            [self cancel_allNotifications];
            state_changed=1;
            
            
            //SCHEDULING NOTIFICATION  GO BACK TO WORK WORK
            [self send_healthwarning:@"Working time, Go back to your desk!"];
//            

        }
        
    }
    else if([region.identifier isEqualToString:@"Door_Beacon2_Region" ])
    {
        if(state==1 && [s retrieveDoor1ExitStatus] )// if current_state==outsideoffice && door_1exit==true
        {
            [s setCurrentStatus:0];//@"inside office"
            
            [self cancel_allNotifications];
             state_changed=1;
            
            
            //SCHEDULING NOTIFICATION  GO BACK TO WORK WORK
            [self send_healthwarning:@"Working time, Go back to your desk!"];

        }
        [s setDoor2ExitStatus:TRUE];
        [s setDoor2EnterStatus:FALSE];
        //door_2enter=true;
    }
    else //region ==door1
    {
        if(([s retrieveDoor2ExitStatus]) && ( [s retrieveCurrentStatus]!=1 ))// if current_state==inside && door_2exit==true
        {
    
            
            
            [s setCurrentStatus:1];//@"outside office"
            state_changed=1;
            [self cancel_allNotifications];
            
            //SCHEDULING NOTIFICATION  GO BACK TO WORK @"Yoy are Outside since last 2 hour,Go Back to Work"
              [self send_healthwarning:@"Working time, Go back !"];
            
            
        }
        [s setDoor1ExitStatus:TRUE];//door_2enter=true;
    }
    
    [s saveToFile:s];//save file

    if (state_changed==1  ) {
      //  UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        SettingsControl *sc=[[SettingsControl alloc]initialize];
        if(![sc retrieveDbStatus])
            self.navBar.backgroundColor=[s retrieve_tintcolor];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change" object:self];
        //
    }
    
    
    /*//Logic for State Change
     fetch current state
     fetch region
     if region==desk
     state_change("insideoffice");
     
     else if region==door_2
     if current_state==outsideoffice && door_1exit==true
     state_change("inside");
     door_2exit=true;
     
     elseif region==door_1
     if current_state==insideoffice && door_2exit==true
     state_change("outside");
     door_2enter=true;
     
     */
}

//-------------------------------------------------------------


-(void)beaconManager:(id)manager didDetermineState:(CLRegionState)state forRegion:(CLBeaconRegion *)region
{
    


}
-(void)beaconManager:(id)manager didStartMonitoringForRegion:(CLBeaconRegion *)region{

}
-(void)beaconManagerDidStartAdvertising:(id)manager error:(NSError *)error{
    
}

-(BOOL)discard_exitnotification
{
    BOOL returnvalue=false;
    Status *s=[[Status alloc]init];
    s=[s initialize];

    NSDate *lastexited=[s retrieve_lastDeskexit];
    lastexited=[lastexited dateByAddingTimeInterval:60];

    if ( ([[NSDate date] compare:lastexited])==NSOrderedAscending) {
        
        returnvalue= true;
    }
    [s set_lastDeskexit:[NSDate date]];
    [s saveToFile:s];
    return returnvalue;
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

//      [UIApplication sharedApplication].applicationIconBadgeNumber =   0;
//      [self setBadgenumber];
//
//    Status *st = [[Status alloc]init];
//    st=[st initialize];
//    [st setCurrentStatus:[st retrieveCurrentStatus]];
//    [st saveToFile:st];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"sate_change"  object:self];

    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
       [self saveContext];
}
- (IBAction)panaction:(id)sender {
    CGPoint translation=[self.pan translationInView:self.window];
    if((self.slidingMenu.center.x+translation.x)<0.4*screenWidth5){
        self.slidingMenu.center=CGPointMake(self.slidingMenu.center.x+translation.x,self.slidingMenu.center.y);
        [self.pan setTranslation:CGPointMake(0,0) inView:self.window];
        [UIView animateWithDuration:0.6 animations:^{
        }
                         completion:^(BOOL finished){}];
    }
    if(self.pan.state == UIGestureRecognizerStateEnded)
    {
        if(self.slidingMenu.center.x<-0.2*screenWidth5){
            [UIView animateWithDuration:0.2 animations:^{
                self.slidingMenu.center=CGPointMake(-0.4*screenWidth5,0.5*screenHeight5);
                
            }
                             completion:^(BOOL finished){}];
            
        }else{
            [UIView animateWithDuration:0.3 animations:^{
                self.slidingMenu.center=CGPointMake(0.4*screenWidth5,0.5*screenHeight5);
            }
                             completion:^(BOOL finished){}];
            self.veilView.hidden=false;
            
        }
    }
    NSLog(@"Entered function");
}
- (IBAction)slidingmenuback:(id)sender {
    [UIView animateWithDuration:.3 animations:^{
        self.slidingMenu.center=CGPointMake(-0.4*screenWidth5,0.5*screenHeight5);
    }
                     completion:^(BOOL finished){}];
    self.veilView.hidden=true;
    
}

- (IBAction)slidingmenucome:(id)sender {

    [UIView animateWithDuration:0.3 animations:^{
        self.slidingMenu.center=CGPointMake(0.4*screenWidth5,0.5*screenHeight5);
    }
                     completion:^(BOOL finished){}];
    self.veilView.hidden=false;
}








#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "beaconapp.qburst.com.ertert" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ertert" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ertert.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
