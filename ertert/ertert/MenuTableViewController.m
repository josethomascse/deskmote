//
//  MenuTableViewController.m
//  ertert
//
//  Created by user on 6/25/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "MenuTableViewController.h"
#import "SplitCell.h"
#import "ViewController.h"
#import "SettingsControl.h"
#import "Status.h"
#import "AppDelegate.h"

@interface MenuTableViewController ()
@property NSMutableArray *splitrecord;
@end

@implementation MenuTableViewController{
}
static NSString *CellIdentifier = @"Cell";

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
    
}


AppDelegate *appDelegate;
- (void)viewDidLoad {
   appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    [super viewDidLoad];
    
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"BACKGROUND.jpg"]];
    self.view1.backgroundColor = background;
    
    SettingsControl *obj = [[SettingsControl alloc]init];
    obj=[obj initialize];
    [_username setText:[obj retrieveusername]];
    [_companyname setText:[obj retrievecompanyname]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:[obj retrieveProfilePic]]){
        _profileimage.image = [UIImage imageWithContentsOfFile:[obj retrieveProfilePic]];
    }
    _profileimage.layer.cornerRadius = _profileimage.frame.size.height /2;
    _profileimage.layer.masksToBounds = YES;
    
    UISwipeGestureRecognizer *swipeLeft;
    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
-(void)viewDidAppear:(BOOL)animated{
    SettingsControl *obj = [[SettingsControl alloc]init];
    obj = [obj initialize];
    [_username setText:[obj retrieveusername]];
    [_companyname setText:[obj retrievecompanyname]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:[obj retrieveProfilePic]]){
        
        
        _profileimage.image = [UIImage imageWithContentsOfFile:[obj retrieveProfilePic]];
    }

    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
    // Return the number of rows in the section.
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier=@"Cell";
    SplitCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    switch(indexPath.row){
        case 0: [cell.title setText:[NSString stringWithFormat:@"Home"]];
            cell.image.image=[UIImage imageNamed:@"home.png"];
            cell.contentView.backgroundColor=[UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
            if(self.selected==0){
                cell.image.image=[UIImage imageNamed:@"home_active.png"];
                cell.contentView.backgroundColor=[UIColor colorWithRed:37.0/255.0 green:42.0/255.0 blue:48.0/255.0 alpha:1];
            }
         break;
        case 1:cell.title.text=[NSString stringWithFormat:@"Statistics"];
            cell.image.image=[UIImage imageNamed:@"graph.png"];
            cell.contentView.backgroundColor=[UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
            if(self.selected==1){
                cell.image.image=[UIImage imageNamed:@"graph_active.png"];
                 cell.contentView.backgroundColor=[UIColor colorWithRed:37.0/255.0 green:42.0/255.0 blue:48.0/255.0 alpha:1];
            }
            break;
        case 2:cell.title.text=[NSString stringWithFormat:@"Reminders"];
            cell.image.image=[UIImage imageNamed:@"reminder.png"];
            cell.contentView.backgroundColor=[UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
            if(self.selected==2){
                cell.image.image=[UIImage imageNamed:@"reminder_active.png"];
                 cell.contentView.backgroundColor=[UIColor colorWithRed:37.0/255.0 green:42.0/255.0 blue:48.0/255.0 alpha:1];        }
            break;
        case 3:cell.title.text=[NSString stringWithFormat:@"Profile"];
            cell.image.image=[UIImage imageNamed:@"profile.png"];
            cell.contentView.backgroundColor=[UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
            if(self.selected==3){
                cell.image.image=[UIImage imageNamed:@"profile_active.png"];
                cell.contentView.backgroundColor=[UIColor colorWithRed:37.0/255.0 green:42.0/255.0 blue:48.0/255.0 alpha:1];
            }
            break;
        case 4:cell.title.text=[NSString stringWithFormat:@"Settings"];
            cell.image.image=[UIImage imageNamed:@"settings.png"];
            cell.contentView.backgroundColor=[UIColor colorWithRed:41.0/255 green:48.0/255.0 blue:48.0/255.0 alpha:1];
            if(self.selected==4){
                cell.image.image=[UIImage imageNamed:@"settings_active.png"];
                cell.contentView.backgroundColor=[UIColor colorWithRed:37.0/255.0 green:42.0/255.0 blue:48.0/255.0 alpha:1];            }
            break;
        default:cell.title.text=[NSString stringWithFormat:@"Error"];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [CATransaction setCompletionBlock:^{
    int prev=self.selected;
    self.selected=(int)indexPath.row;
    if((prev==self.selected)&&(self.selected!=2))
        return;
    [self.tableView reloadData];
    UIViewController *aYourViewController;
    switch(self.selected)
    {
        case 0:
            aYourViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"Home"];
                                  break;
            
        case 1:
            aYourViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"Statistics"];
                break;

        case 2:
            aYourViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"Reminder"];
            break;
            
        case 3:
            aYourViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"Profile"];
            break;
            
        case 4:
            aYourViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"Settings"];
            break;
    }
        
    [self.view.window setRootViewController:aYourViewController ];
    }];
      [appDelegate slidingmenuback:self];
}
-(void)handleSwipeLeft{
    [appDelegate slidingmenuback:self];
}


@end
