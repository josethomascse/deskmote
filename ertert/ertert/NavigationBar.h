//
//  NavigationBar.h
//  deskmote
//
//  Created by user on 7/30/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationBar : UIView

@property (retain,nonatomic) UILabel *titleLabel;
-(void)initialize;
@end
