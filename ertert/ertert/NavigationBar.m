//
//  NavigationBar.m
//  deskmote
//
//  Created by user on 7/30/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "NavigationBar.h"

@implementation NavigationBar


CGRect screenRect6;
CGFloat screenWidth6;

/*
- (void)drawRect:(CGRect)rect {
    
      // Drawing code
}
*/
-(void)initialize{
    screenRect6 = [[UIScreen mainScreen] bounds];
    screenWidth6 = screenRect6.size.width;
    
    self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0.3*screenWidth6, 22, 0.4*screenWidth6,40)];
    [self.titleLabel setFont:[UIFont systemFontOfSize:18]];
    self.titleLabel.text=@"Home";
    self.titleLabel.textColor=[UIColor blackColor];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:self.titleLabel];

}
@end
