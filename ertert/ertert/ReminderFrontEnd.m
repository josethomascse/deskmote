//
//  ReminderFrontEnd.m
//  deskmote
//
//  Created by user on 7/30/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "ReminderFrontEnd.h"
#import "AppDelegate.h"
#import "ToDoListTableViewController.h"
#import "DatePickerViewController.h"
@interface ReminderFrontEnd ()

@end

@implementation ReminderFrontEnd

- (void)viewDidLoad {
    
    AppDelegate *appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    
    [self.view addSubview:appDelegate.navBar];
    appDelegate.navBar.titleLabel.text=@"Reminder";
    [self.view addSubview:appDelegate.slidingMenu];
    
    [self.view addSubview:appDelegate.veilView];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ToDoListTableViewController *controller= [storyboard instantiateViewControllerWithIdentifier:@"ReminderView"];
    controller.view.frame = self.mainView.bounds;
    [self.mainView addSubview:controller.view];
    
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    
    [self addChildViewController:appDelegate.table];
    [appDelegate.table didMoveToParentViewController:self];
   
    CGRect screenRect2 = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth2 = screenRect2.size.width;
    
    
    UIButton *rightSideButton = [[UIButton alloc] init];
    rightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightSideButton.frame = CGRectMake(0.9*screenWidth2 , 30, 26, 26);
    [rightSideButton setBackgroundImage:[UIImage imageNamed:@"add_reminder.png"]
                               forState:UIControlStateNormal];
    
    [rightSideButton addTarget:self action:@selector(addreminder:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightSideButton];

    [super viewDidLoad];
    
}

- (IBAction)addreminder:(id)sender {
    
    
    DatePickerViewController *popController1 = [[DatePickerViewController alloc] init];
    popController1.sourceView = sender;
    popController1.contentSize = CGSizeMake(300, 45);
    popController1.arrowDirection = 0;
    popController1.flag=1;
    [self presentViewController:popController1
                       animated:YES
                     completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
