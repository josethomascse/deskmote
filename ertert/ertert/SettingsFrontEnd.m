//
//  SettingsFrontEnd.m
//  deskmote
//
//  Created by user on 7/30/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "SettingsFrontEnd.h"
#import "AppDelegate.h"
#import "SettingsViewController.h"

@interface SettingsFrontEnd ()

@end

@implementation SettingsFrontEnd

- (void)viewDidLoad {
    AppDelegate *appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    
    [self.view addSubview:appDelegate.navBar];
    appDelegate.navBar.titleLabel.text=@"Settings";
    [self.view addSubview:appDelegate.slidingMenu];
    
    [self.view addSubview:appDelegate.veilView];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     SettingsViewController *controller= [storyboard instantiateViewControllerWithIdentifier:@"SettingsView"];
    controller.view.frame = self.mainView.bounds;
    [self.mainView addSubview:controller.view];
    
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
    
    [self addChildViewController:appDelegate.table];
    [appDelegate.table didMoveToParentViewController:self];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
