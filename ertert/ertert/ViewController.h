//
//  ViewController.h
//  ertert
//
//  Created by user on 6/8/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircleProgressView;

@interface ViewController : UIViewController<UISplitViewControllerDelegate>




@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (strong, nonatomic) IBOutlet UIButton *actionButton;
@property (strong, nonatomic) IBOutlet CircleProgressView *circleProgressView;

@property (weak, nonatomic) IBOutlet UITextField *time1;

@property (weak, nonatomic) IBOutlet UITextField *time2;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *back;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *timer1;
@property (strong, nonatomic) IBOutlet UILabel *timer2;
@property (nonatomic, retain) NSDate * startDate;
@property (strong, nonatomic) IBOutlet UILabel *status_label;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;

@end

