//
//  ViewController.m
//  ertert
//
//  Created by user on 6/8/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "ViewController.h"
#import "CircleProgressView.h"
//import "Session.h"
#import "Status.h"
#import "StatisticsOperations.h"
#import "SettingsControl.h"
#import "DatePickerViewController.h"
#import "AppDelegate.h"


@interface ViewController ()
    {
        struct time tempTime;
        NSString *image_name;
        int prev;
        BOOL savedb;
        BOOL observerexist;
    }

@property (strong, nonatomic) NSTimer *timer;
@property (strong,nonatomic) AppDelegate *appDelegate;

//@property (nonatomic) Session *session;


@end

@implementation ViewController
UIView *myView;
bool colour=1;

- (void)viewDidLoad {
     [super viewDidLoad];
    
    CGRect screenRect2 = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth2 = screenRect2.size.width;
    

//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"ic_menu.png"];
//    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"ic_menu.png"];
//    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
//    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
//    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"ic_menu.png"]];
//    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage :[UIImage imageNamed:@"ic_menu.png"]];
    
    self.appDelegate=( AppDelegate* )[UIApplication sharedApplication].delegate;
    
    [self.view addSubview:self.appDelegate.navBar];
    self.appDelegate.navBar.titleLabel.text=@"Home";
    [self.view addSubview:self.appDelegate.slidingMenu];
    [self.view addSubview:self.appDelegate.veilView];
    [self addChildViewController:self.appDelegate.table];
    [self.appDelegate.table didMoveToParentViewController:self];
    
    UIButton *rightSideButton = [[UIButton alloc] init];
    rightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightSideButton.frame = CGRectMake(0.9*screenWidth2 , 30, 26, 26);
    [rightSideButton setBackgroundImage:[UIImage imageNamed:@"add_reminder.png"]
                                    forState:UIControlStateNormal];
    
    [rightSideButton addTarget:self action:@selector(addreminder:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightSideButton];


    //[self.view addGestureRecognizer:appDelegate.tap];

    
   
//    
//    [self.appDelegate slidingmenuback:self];

    [UIApplication sharedApplication].applicationIconBadgeNumber =   0;


    [self animate_statechange];
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animate_statechange) name:@"sate_change" object:nil];
    [self animate_statechange];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sate_change" object:nil];
}


//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"sate_change"
//                                                  object:nil];
//}

//-(void)schedule_AppstartStop:(NSString *)identifier
//{
//    SettingsControl *sc=[SettingsControl new];
//    UILocalNotification *localNotification=[UILocalNotification new];
//    localNotification.fireDate=   sc.retrieveOfficeStart;
//    localNotification.timeZone = [NSTimeZone defaultTimeZone];
//    NSDictionary *info=[NSDictionary dictionaryWithObject:@"nothing" forKey:identifier];
//    localNotification.userInfo=info;
//   [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
// }



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 #pragma mark - Timer
- (void)startTimer {
    if ((!self.timer) || (![self.timer isValid]) ) {
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.00
                                                      target:self
                                                    selector:@selector(poolTimer)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)poolTimer
{
    self.circleProgressView.elapsedTime =  [[NSDate date] timeIntervalSinceDate:self.startDate]+prev;
//self.session.progressTime;
}

-(UIImage *)convertImageToGrayScale:(UIImage *)image {
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace,(CGBitmapInfo) kCGImageAlphaNone);
    CGContextDrawImage(context, imageRect, [image CGImage]);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    context = CGBitmapContextCreate(nil,image.size.width, image.size.height, 8, 0, nil, (CGBitmapInfo)kCGImageAlphaOnly );
    CGContextDrawImage(context, imageRect, [image CGImage]);
    CGImageRef mask = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:CGImageCreateWithMask(imageRef, mask)];
    CGImageRelease(imageRef);
    CGImageRelease(mask);
    return newImage;
}


#pragma mark - User Interaction

- (void)progress_animation_state:(int)state {
    
//    if (self.session.state == kSessionStateStop) {
    UIColor *tintColor;
    if (0==state) {
        tintColor = [UIColor colorWithRed:129/255.0 green:199/255.0 blue:132/255.0 alpha:1.0];
    } else if (1==state){
       
          tintColor = [UIColor colorWithRed:255/255.0 green:213/255.0 blue:79/255.0 alpha:1.0];           }
    else
    {
      
         tintColor = [UIColor colorWithRed:77/255.0 green:208/255.0 blue:225/255.0 alpha:1.0];
    }
    
   //     self.startDate = [NSDate date];
//        self.session.finishDate = nil;
//        self.session.state = kSessionStateStart;
    
    
        self.circleProgressView.status = NSLocalizedString(@"circle-progress-view.status-in-progress", nil);
        self.circleProgressView.tintColor = tintColor;
    Status *s=[[Status alloc]init];
    s=[s initialize];
    SettingsControl *st=[[SettingsControl alloc]init];
    st=[st initialize];
    if([st retrieveDbStatus]){
        StatisticsOperations *st=[[StatisticsOperations alloc]init];
        struct statObject stat=[st retrieveTimeObject:[NSDate date] seconds:1 realTime:0];
        switch ([s retrieveCurrentStatus]) {
            case 0:
                prev=stat.inside.seconds;
                break;
            case 1:
                prev=stat.outside.seconds;
                break;
            case 2:
                prev=stat.desk.seconds;
                break;
                
            default:
                break;
        }
         self.circleProgressView.elapsedTime = (prev/60)*60;
        [self.timer invalidate];
        self.image1.image = [self convertImageToGrayScale:_image1.image];
        [self.image1 setAccessibilityIdentifier:nil];
        self.image2.image = [self convertImageToGrayScale:_image2.image];
         [self.image2 setAccessibilityIdentifier:nil];
        self.image3.image = [self convertImageToGrayScale:_image3.image];
         [self.image3 setAccessibilityIdentifier:nil];
        self.navigationController.navigationBar.barTintColor=[UIColor whiteColor];
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];

    }else{
        self.circleProgressView.elapsedTime = 0;
        [self startTimer];
    }
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController
     willHideViewController:(UIViewController *)viewController withBarButtonItem:
(UIBarButtonItem *)barButtonItem forPopoverController:
(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
   
}

- (void)splitViewController:(UISplitViewController *)splitController
     willShowViewController:(UIViewController *)viewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view,
    //invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
   
}

-(void)animate_statechange{
    
    Status *s=[[Status alloc]init];
    s=[s initialize];
    [s saveToFile:s];
    int current_state=[s retrieveCurrentStatus];
    int pvs_state=[s retrievePvsStatus];

    self.navigationController.navigationBar.layer.cornerRadius = 6;
    self.navigationController.navigationBar.layer.masksToBounds = NO;
    self.navigationController.navigationBar.layer.borderWidth = 0;
    self.navigationController.navigationBar.translucent=NO;
    [ self.navigationController.navigationBar.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [ self.navigationController.navigationBar.layer setShadowOpacity:1];
    [ self.navigationController.navigationBar.layer setShadowRadius:1.2];
    [ self.navigationController.navigationBar.layer setShadowOffset:CGSizeMake(0.0, 1.5)];
    switch (current_state) {
     
        case 0:
            _status.text=@"INSIDE OFFICE";
            self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:129/255.0 green:199/255.0 blue:132/255.0 alpha:1.0];
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:129/255.0 green:199/255.0 blue:132/255.0 alpha:1.0]];
            
     
            break;
        case 1:
            _status.text=@"OUTSIDE OFFICE";
             self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:255/255.0 green:213/255.0 blue:79/255.0 alpha:1.0];
        
     
                [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:255/255.0 green:213/255.0 blue:79/255.0 alpha:1.0]];
            break;
        case 2:
            _status.text=@"AT WORK";
             self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:77/255.0 green:208/255.0 blue:225/255.0 alpha:1.0];
            [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:77/255.0 green:208/255.0 blue:225/255.0 alpha:1.0]];
         
     
               break;
        default:
            break;
    }
    
    //        inside_office=0,
    //        outside_office=1,
    //        work=2
    
    
    
    NSString *identifier=[_image1 accessibilityIdentifier];
    
    if (identifier==nil) {
        [self initialise_state:current_state large:1];
        [self assignto_image:1];
        [self initialise_state:pvs_state large:0];
        [self assignto_image:2];
        [self initialise_state:3-(current_state+pvs_state) large:0];
        [self assignto_image:3];
        [self progress_vew];
        [self progress_animation_state:current_state];
        return;
    }
    [self initialise_state:current_state large:0];
    if ([[_image2 accessibilityIdentifier] isEqualToString:image_name])
    {
      

            [CATransaction setCompletionBlock:^{
                [CATransaction setCompletionBlock:^{
                    [CATransaction setCompletionBlock:^{
                        [self progress_vew];
                        [self progress_animation_state:current_state];
                    }];
                    [self initialise_state:3-(current_state+pvs_state) large:0];
                    [self flip_aimation:_image3 :3];
                }];
                [self initialise_state:pvs_state large:0];
                [self flip_aimation:_image2 :2];
            }];

            _circleProgressView.tintColor=[UIColor clearColor];
            [self.timer invalidate];
            _circleProgressView.elapsedTime=0;
            [self initialise_state:current_state large:1];
            [self flip_aimation:_image1 :1];
    }
    else  if ([[_image3 accessibilityIdentifier] isEqualToString:image_name]) {
        
   
        [CATransaction setCompletionBlock:^{
            [CATransaction setCompletionBlock:^{
                [CATransaction setCompletionBlock:^{
                    [self progress_vew];
                    [self progress_animation_state:current_state];
                }];
                [self initialise_state:3-(current_state+pvs_state) large:0];
                [self flip_aimation:_image3 :3];
            }];
            [self initialise_state:pvs_state large:0];
            [self flip_aimation:_image2 :2];
        }];
        _circleProgressView.tintColor=[UIColor clearColor];
        [self.timer invalidate];
        _circleProgressView.elapsedTime=0;
        [self initialise_state:current_state large:1];
        [self flip_aimation:_image1 :1];

    }
    else
    {
        [self progress_vew];
        [self progress_animation_state:current_state];
    }
        return;
    
 
    
    
    
    
}



-(void)initialise_state:(int )state large:(int)large
{
    //        inside_office=0,
    //        outside_office=1,
    //        work=2
    StatisticsOperations *sop=[[StatisticsOperations alloc]init ];
    struct statObject stat;
    stat=[sop retrieveTimeObject:[NSDate date] seconds:0 realTime:0];
    switch (state) {
        case 0:
            if (1==large)
                image_name=@"office_large.png";
            else
                image_name=@"office.png";
            tempTime=stat.inside;
        //    prev=stat.inside.seconds;
            break;
            
        case 1:
            if (1==large)
                image_name=@"outdoor_large.png";
            else
                image_name=@"outdoor.png";
            tempTime=stat.outside;
//            prev=stat.outside.seconds;

            break;
        case 2:
            if (1==large)
                image_name=@"work_large.png";
            else
                image_name=@"work.png";
            tempTime=stat.desk;
//            prev=stat.desk.seconds;
            break;
            
        default:
            break;
    }
    
    
}



-(void)assignto_image:(int)image

{
    switch (image) {
        case 1:
            
            _image1.image=[UIImage imageNamed:image_name];
            [_image1 setAccessibilityIdentifier:image_name];
            //  _timer1.text=[NSString stringWithFormat:@"%d:%d",tempTime.hours,tempTime.minutes];
            break;
            
        case 2:
            _image2.image=[UIImage imageNamed:image_name];
            [_image2 setAccessibilityIdentifier:image_name];
            _time1.text=[NSString stringWithFormat:@"%02d:%02d",tempTime.hours,tempTime.minutes];
            _time1.adjustsFontSizeToFitWidth=YES;
            break;
            
        case 3:
            _image3.image=[UIImage imageNamed:image_name];
            [_image3 setAccessibilityIdentifier:image_name];
            _time2.text=[NSString stringWithFormat:@"%02d:%02d",tempTime.hours,tempTime.minutes];
            _time2.adjustsFontSizeToFitWidth=YES;

            break;
            
        default:
            break;
    }
    
    
}

-(void)progress_vew{
    
    StatisticsOperations *sop=[[StatisticsOperations alloc]init ];
    struct statObject stat;
    stat=[sop retrieveTimeObject:[NSDate date] seconds:1 realTime:0];
    
    Status *s;
    s=[[Status alloc]init];
    s=[s initialize];
    self.startDate = [s getCurrentStartDate];
    _image1.layer.masksToBounds = NO;
    _image1.layer.borderWidth = 0;
    
    
//    self.session = [[Session alloc] init];
//    self.session.state = kSessionStateStop;
    
    self.circleProgressView.status = NSLocalizedString(@"circle-progress-view.status-not-started", nil);
    self.circleProgressView.timeLimit = 60;
    //self.circleProgressView.elapsedTime = [s getCurrentStatusDuration];
    switch ([s retrieveCurrentStatus]) {
        case 0:
                prev=stat.inside.seconds;
            break;
        case 1:
                prev=stat.outside.seconds;
            break;
        case 2:
                prev=stat.desk.seconds;
            break;
            
        default:
            break;
    }
    
   

}

-(void)translation_animation:(int)image :(int)currentstate
{


 
    CGRect screenRect= [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight= screenRect.size.height;
    
    CGPoint image1origin=CGPointMake(_image1.frame.origin.x,_image1.frame.origin.y);
        CGPoint progressview=CGPointMake(_circleProgressView.frame.origin.x,_circleProgressView.frame.origin.y);
    CGPoint image2origin;
    int width,height;
    
    
    switch (image) {
        case 2:
              [_image1 setFrame:CGRectMake(_image2.frame.origin.x-_image1.frame.size.width/2,_image2.frame.origin.y-_image1.frame.size.height , _image1.frame.size.width, _image1.frame.size.height)];
               [_circleProgressView setFrame:CGRectMake(_image2.frame.origin.x-_image2.frame.size.width,_image2.frame.origin.y , _circleProgressView.frame.size.width, _circleProgressView.frame.size.height)];
            
            
            image2origin=CGPointMake(_image2.frame.origin.x,_image2.frame.origin.y);
             [_image2 setFrame:CGRectMake(screenWidth/2-_image2.frame.size.width/2,image1origin.y , _image2.frame.size.width, _image2.frame.size.height)];
            width= _image2.frame.size.width;
            height= _image2.frame.size.height;
            break;
        case 3:
              [_image1 setFrame:CGRectMake(_image3.frame.origin.x-_image1.frame.size.width/2,_image3.frame.origin.y-_image1.frame.size.height/2 , _image1.frame.size.width, _image1.frame.size.height)];
              [_circleProgressView setFrame:CGRectMake(_image3.frame.origin.x,_image3.frame.origin.y , _circleProgressView.frame.size.width, _circleProgressView.frame.size.height)];
            
            
            image2origin=CGPointMake(_image3.frame.origin.x,_image3.frame.origin.y);
             [_image3 setFrame:CGRectMake(screenWidth/2-_image3.frame.size.width/2,image1origin.y , _image3.frame.size.width, _image3.frame.size.height)];
            width= _image3.frame.size.width;
            height= _image3.frame.size.height;
            break;
        default:
            break;
    }
  
    
    
    
  
    

//    
    [UIView animateWithDuration:.7
                        delay:.1
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                             [_circleProgressView setFrame:CGRectMake(progressview.x,progressview.y , _circleProgressView.frame.size.width, _circleProgressView.frame.size.height)];
                         
                         [_image1 setFrame:CGRectMake(image1origin.x,image1origin.y , _image1.frame.size.width, _image1.frame.size.height)];
                         
                         _image1.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
                         _image1.contentMode = UIViewContentModeScaleAspectFit;
                     }
                     completion:^(BOOL finished){
                                            
                     }];
 

    [UIView animateWithDuration:.2
                          delay:.1
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                         switch (image) {
                             case 2:
                                 [_image2 setFrame:CGRectMake(screenWidth/2-_image2.frame.size.width/2, screenHeight-_image2.frame.size.height/2, width, height)];
                                 
                                 break;
                                
                             case 3:
                                 [_image3 setFrame:CGRectMake(screenWidth/2-_image3.frame.size.width/2, screenHeight-_image2.frame.size.height/2, width, height)];
                                 
                             default:
                                 break;
                         }
                         
  
                            }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                        
                         
                     }];
    [UIView animateWithDuration:1
                          delay:.3
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                         switch (image) {
                             case 2:
                                  [_image2 setFrame:CGRectMake(image2origin.x,image2origin.y , width, height)];
                                 break;
                             case 3:
                                 [_image3 setFrame:CGRectMake(image2origin.x,image2origin.y , width, height)];
                                 
                             default:
                                 break;
                         }
                        
                         
                     }
                     completion:^(BOOL finished){
             
                  
                     }];



}
-(void)flip_aimation:(UIImageView *)image :(int)imagenummber
{

    [UIView transitionWithView:image
                      duration:0.4
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{

                        [self assignto_image:imagenummber];
                    } completion:^(BOOL finished) {
                        //  Do whatever when the animation is finished
                    }];
//    [UIView animateWithDuration:2 delay:0.0 options:UIViewAnimationOptionTransitionFlipFromLeft
//                     animations:^(void) {
//                         image.transform = CGAffineTransformMakeScale(1, 1);
//                      }
//     
//                     completion:nil];
    
    
//    [UIView animateWithDuration:0.5 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut
//                     animations:^(void) {
//                         image.transform = CGAffineTransformMakeScale(direction, 1);
//                     }
//     
//                     completion:nil];
    
 


}


-(void)shake:(UIView *)image
{
    
    CGPoint textorigin=CGPointMake(image.frame.origin.x,image.frame.origin.y);
    
    float delay=0.2;
    for (int i=0; i<8; i++) {
  
    [UIView animateWithDuration:.2
                          delay:delay*i
                        options: UIViewAnimationOptionAutoreverse
     
                     animations:^ {
                         [image setFrame:CGRectMake(textorigin.x+6, textorigin.y, image.frame.size.width, image.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
       
                         
                     }];
    [UIView animateWithDuration:.2
                          delay:(delay*i)+.2
                        options: UIViewAnimationOptionTransitionCurlDown
     
                     animations:^ {
                         [image setFrame:CGRectMake(textorigin.x,textorigin.y , image.frame.size.width, image.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
               
                         
                     }];
        
        
    }

    
    
}

- (IBAction)button:(id)sender {
  //  [self rotating_animation:0];
}

- (IBAction)test:(id)sender {
    Status *s=[[Status alloc]init];
    s=[s initialize];
    [s setCurrentStatus:2];
    [s saveToFile:s];
    [self animate_statechange];
}

- (IBAction)addreminder:(id)sender {
    
    
    DatePickerViewController *popController1 = [[DatePickerViewController alloc] init];
    popController1.sourceView = sender;
    popController1.contentSize = CGSizeMake(300, 45);
    popController1.arrowDirection = 0;
        popController1.flag=1;
    [self presentViewController:popController1
                       animated:YES
                     completion:nil];
    
}

@end
