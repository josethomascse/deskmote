//
//  profileViewController.m
//  ertert
//
//  Created by user on 6/23/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import "profileViewController.h"
#import "SettingsControl.h"
#import "Status.h"
#import "AppDelegate.h"
@interface profileViewController ()<UIAlertViewDelegate,UITextFieldDelegate>
@property (nonatomic, retain) NSString *p1,*p2,*imagepath;
@end

@implementation profileViewController
@synthesize username,companyname;
AppDelegate *appDelegate2;
NSString *np1,*np2;
NSInteger flag=0;
UIImage *img;
- (void)viewDidLoad {
    CGRect screenRect2 = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth2 = screenRect2.size.width;
    
    appDelegate2=( AppDelegate* )[UIApplication sharedApplication].delegate;
    
    [self.view addSubview:appDelegate2.navBar];
    appDelegate2.navBar.titleLabel.text=@"Profile";
    [self.view addSubview:appDelegate2.slidingMenu];
    [self.view addSubview:appDelegate2.veilView];
    [self addChildViewController:appDelegate2.table];
    [appDelegate2.table didMoveToParentViewController:self];
    
    UIButton *rightSideButton = [[UIButton alloc] init];
    rightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightSideButton.frame = CGRectMake(0.88*screenWidth2 , 25, 46, 36);
    [rightSideButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightSideButton setTitle:@"Edit" forState:UIControlStateNormal];
    rightSideButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [rightSideButton addTarget:self action:@selector(edit:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightSideButton];
    
    [super viewDidLoad];
    flag=0;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"ic_menu.png"];
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"ic_menu.png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        
        
        _profile1.image = [UIImage imageWithContentsOfFile:path];
    }

   
    _profile1.layer.cornerRadius = _profile1.frame.size.height/2;
    _profile1.layer.masksToBounds = YES;
    _profile1.layer.borderWidth = 2.0f;
    
    self.profile1.layer.borderColor = [UIColor whiteColor].CGColor;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loadstring1 = [defaults objectForKey:@"savedstring1"];
    [username setText:loadstring1];
    NSString *loadstring2 = [defaults objectForKey:@"savedstring2"];
    [companyname setText:loadstring2];
   username.delegate=self;
   companyname.delegate=self;
    CGRect screenRect;
    CGFloat screenCenterX,screenCenterY;
    screenRect = [[UIScreen mainScreen] bounds];
    screenCenterX = screenRect.size.width/2;
    screenCenterY = screenRect.size.height/2;
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenCenterX-105,companyname.center.y+11.5, 210, 25)];
    [self setToastLabel:tempLabel];
    [self.ToastLabel setFont:[UIFont fontWithName:@"Avenir-Roman" size:13]];
    [self.ToastLabel  setTextAlignment:NSTextAlignmentCenter];
    [self.ToastLabel  setTextColor:[UIColor whiteColor]];
    self.ToastLabel.backgroundColor =[UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
    self.ToastLabel.layer.cornerRadius = 10;
    
    self.ToastLabel.layer.masksToBounds = YES;
    [_ToastLabel setHidden:YES];
    [[self view] addSubview:_ToastLabel];

    
}


- (IBAction) pickImage:(id)sender{
    
    UIImagePickerController *pickerController = [[UIImagePickerController alloc]
                                                 init];
    pickerController.delegate = self;
    [self presentViewController:pickerController animated:YES completion:nil];
}


#pragma mark UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker
         didFinishPickingImage:(UIImage *)image
                   editingInfo:(NSDictionary *)editingInfo
{
   
    if (image != nil)
    {
        img=[self scaleAndRotateImage: image];
    }
    self.profile1.image = image;
    self.profile1.layer.cornerRadius = self.profile1.frame.size.width / 2;
    self.profile1.clipsToBounds = YES;
    self.profile1.layer.borderWidth = 3.0f;
    self.profile1.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self dismissModalViewControllerAnimated:YES];
    [username becomeFirstResponder];
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    int kMaxResolution = 320;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)animate:(NSTimer *)theTimer {
    [_ToastLabel setHidden:YES];
}

- (IBAction)edit:(id)sender {
    flag=1;
    _picedit.hidden=NO;
    _cancel.hidden=NO;
    _save.hidden=NO;
    username.enabled=YES;
    companyname.enabled=YES;
    [username becomeFirstResponder];
}

- (IBAction)textfieldvaluechanged:(id)sender {
    _cancel.hidden=NO;
    _save.hidden=NO;
    NSString *text = nil;
    int MAX_LENGTH = 20;
    switch ([sender tag] )
    {
        case 0:
        {
            text = username.text;
            if (MAX_LENGTH < [text length]) {
                username.text = [text substringToIndex:MAX_LENGTH];
                        [_ToastLabel setText:@"Exceeded Character number!!"];
                        [_ToastLabel setHidden:NO];
                        [NSTimer scheduledTimerWithTimeInterval:1.5
                                                         target:self
                                                       selector:@selector(animate:)
                                                       userInfo:nil
                                                        repeats:NO];
            }
        }
            break;
        case 1:
        {
            text = companyname.text;
            MAX_LENGTH = 25;
            if (MAX_LENGTH < [text length]) {
                companyname.text = [text substringToIndex:MAX_LENGTH];
                [_ToastLabel setText:@"Exceeded Character number!!"];
                [_ToastLabel setHidden:NO];
                [NSTimer scheduledTimerWithTimeInterval:1.5
                                                 target:self
                                               selector:@selector(animate:)
                                               userInfo:nil
                                                repeats:NO];
            }
        }
        default:
            break;
    }
    
}
- (IBAction)Cancelaction:(id)sender {
    _picedit.hidden=YES;
    _cancel.hidden=YES;
    _save.hidden=YES;
    username.enabled=FALSE;
    companyname.enabled=FALSE;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    _profile1.image = [UIImage imageWithContentsOfFile:path];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loadstring1 = [defaults objectForKey:@"savedstring1"];
    [username setText:loadstring1];
    NSString *loadstring2 = [defaults objectForKey:@"savedstring2"];
    [companyname setText:loadstring2];
    flag=0;
    
}
- (IBAction)Saveaction:(id)sender {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    NSData* data = UIImagePNGRepresentation(img);
    
    SettingsControl *obj = [[SettingsControl alloc]init];
    obj = [obj initialize];
    [obj setProfilePic:path];
    [obj saveToFile:obj];
    [data writeToFile:path atomically:YES];
    
    
       [self.view addSubview:appDelegate2.slidingMenu];
    [_ToastLabel setText:@"Save Successfull!!"];
    [_ToastLabel setHidden:NO];
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(animate:)
                                   userInfo:nil
                                    repeats:NO];
    _picedit.hidden=YES;
    _cancel.hidden=YES;
    _save.hidden=YES;
    _p1 = username.text;
    _p2 = companyname.text;
    username.enabled=FALSE;
    companyname.enabled=FALSE;
    
    [obj setusername:_p1];
    [obj setcompanyname:_p2];
    [obj saveToFile:obj];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_p1 forKey:@"savedstring1"];
    [defaults setObject:_p2 forKey:@"savedstring2"];
    [defaults synchronize];
    flag=0;
    self.picedit.hidden=YES;
    [appDelegate2.slidingMenu removeFromSuperview];
    [self.view addSubview:appDelegate2.slidingMenu];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}
- (IBAction)dismisskeyboard:(id)sender {
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(flag==1)
    {
    
    return YES;
    }
    else
        return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == username)
    {
        [companyname becomeFirstResponder];
    }
    else
    {
     [textField resignFirstResponder];
    }
    return YES;
}


@end
