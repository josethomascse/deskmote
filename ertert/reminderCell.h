//
//  reminderCell.h
//  ertert
//
//  Created by user on 6/29/15.
//  Copyright (c) 2015 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface reminderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *alert;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UIView *view;

@end
